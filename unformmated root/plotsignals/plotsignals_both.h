#ifndef __PLOTSIGNALS_BOTH
#define __PLOTSIGNALS_BOTH

/* plotsignals_graph.h

	-Header gerenates data and outputs it to a histogram and creates the graphs one at a time
	Created By: Daniel Kulas

*/

#include "plotsignals_functions.h"
#include <fstream>

void plotsignals_both(int display, const char *theFile, Double_t userFreq)
{

    ifstream fp;
    fp.open(theFile);

    string line;
    char semn[1];
    unsigned int found = -1;
    float time;
    float amps;
    int n = 0;
    float x[100000];		//values from input file are stored in these arrays
    float y[100000];
    float y2[100000];

    Double_t time_final;


    char *s = new char[1];
    bool doplot = false;

    //traces through input file line by line
    while (!fp.eof())
    {
        getline (fp,line);
        if (line.find("Created") != found && n>0)
        {
            if (line.find("Direct signal") == found)
            {

                setFixedThres();
                /*=====================Initial Graph====================================*/
                c1->Clear();
                c1->Divide(3,2);
                c1->cd(1);
                TGraph* gr = new TGraph(n,x,y);
                gr->GetYaxis()->SetTitle("I(#muA)");
                gr->GetXaxis()->SetTitle("t(#mus)");
                gr->SetTitle("Initial Signal 1");

                if (display == 1)
                {
                    gr->Draw("AL");
                    c1->Update();
                }

                /*======================Initial Graph 2================================*/
                c1->cd(2);
                TGraph* gr2 = new TGraph(n,x,y2);
                gr2->GetYaxis()->SetTitle("I(#muA)");
                gr2->GetXaxis()->SetTitle("t(#mus)");
                gr2->SetTitle("Initial Signal 2");

                if(display == 1)
                {
                    gr2->Draw("AL");
                    c1->Update();
                }
                /*=====================================================================*/
                /*==========================Fill in arrays============================*/
                int firstbin = 3;
                Double_t *xx = new Double_t[n-firstbin];
                Double_t *yy = new Double_t[n-firstbin];
                Double_t *yy2 = new Double_t[n-firstbin];

                for (int ih = 0; ih<n-firstbin; ih++)
                {
                    xx[ih] =x[ih+firstbin];							//Takes in values from the data file used and stores it into these arrays
                    yy[ih] =y[ih+firstbin];
                    yy2[ih] = y2[ih+firstbin];						//yy2 is the same input signal as yy, but with a different random noise
                }

                n = n-firstbin-1;

                /*=====================================================================*/
                /*======================Draw bottom left graph=========================*/

                c1->cd(4);
                Double_t dt = x[n-1] - x[n-2];
                Double_t *y_sig_out_1 = new Double_t[n+1];
                Double_t R = 0.002;

                if(highFilter == true)
                {

                    highpass(yy,n+1,dt,userFreq,R,y_sig_out_1);
                    setMaxVar();
                    setNewThres_sig1(n,y_sig_out_1);
                    thresLine = new TLine(0,getNewThreshold_sig1(),0.25,getNewThreshold_sig1());

                    TGraph* grhp1 = new TGraph( (n+1),xx,y_sig_out_1);
                    grhp1->GetYaxis()->SetTitle("I(#muA)");
                    grhp1->GetXaxis()->SetTitle("t(#mus)");
                    grhp1->SetTitle("Highpass on signal 1");
                    grhp1->GetX();
                }

                if(lowFilter == true)
                {

                    lowpass(yy,n+1,dt,userFreq,y_sig_out_1);
                    setMaxVar();
                    setNewThres_sig1(n,y_sig_out_1);
                    thresLine = new TLine(0,getNewThreshold_sig1(),0.25,getNewThreshold_sig1());
                    Double_t adcValue = adcdigi(n,xx,y_sig_out_1);
                    adcHitLine = new TLine(getadcHit(), 0, getadcHit(), -10);
                    thresLine = new TLine(0,getNewThreshold_sig1(),0.25,getNewThreshold_sig1());

                    TGraph* grhp1 = new TGraph( (n+1),xx,y_sig_out_1);
                    grhp1->GetYaxis()->SetTitle("I(#muA)");
                    grhp1->GetXaxis()->SetTitle("t(#mus)");
                    grhp1->SetTitle("Lowpass on signal 1");
                    grhp1->GetX();
                }
                if(display == 1)
                {
                    grhp1->Draw("AL");//	VR this is not gonna work because grhp1 is not in scope
                    thresLine->Draw();
                    adcHitLine->Draw();
                    baseLine->Draw();
                    c1->Update();
                }

                /*========================= Bottom right Graph=========================*/
                c1->cd(5);
                dt = x[n-1] - x[n-2];
                Double_t *y_sig_out_2 = new Double_t[n+1];
                R = 0.002;

                if(highFilter == true)
                {

                    highpass(yy2,n+1,dt,userFreq,R,y_sig_out_2);
                    setMaxVar();
                    setNewThres_sig2(n,y_sig_out_2);
                    thresLine2 = new TLine(0,getNewThreshold_sig2(),0.25,getNewThreshold_sig2());

                    TGraph* grhp2 = new TGraph( (n+1),xx,y_sig_out_2);
                    grhp2->GetYaxis()->SetTitle("I(#muA)");
                    grhp2->GetXaxis()->SetTitle("t(#mus)");
                    grhp2->SetTitle("Highpass on signal 2");
                    grhp2->GetX();
                }

                if(lowFilter == true)
                {

                    lowpass(yy2,n+1,dt,userFreq,y_sig_out_2);
                    setMaxVar();
                    setNewThres_sig2(n,y_sig_out_2);
                    thresLine2 = new TLine(0,getNewThreshold_sig2(),0.25,getNewThreshold_sig2());

                    TGraph* grhp2 = new TGraph( (n+1),xx,y_sig_out_2);
                    grhp2->GetYaxis()->SetTitle("I(#muA)");
                    grhp2->GetXaxis()->SetTitle("t(#mus)");
                    grhp2->SetTitle("Lowpass on signal 2");
                    grhp2->GetX();
                }

                if(display == 1)
                {
                    grhp2->Draw("AL");	//VR this is not gonna work because grhp1 is not in scope
                    thresLine2->Draw();
                    baseLine->Draw();
                    c1->Update();
                }

                /*=====================================================================*/
                /*================= Histogram of ADC on signal 1 right Graph===========*/
                c1->cd(3);
                dt = x[n-1] - x[n-2];
                Double_t *y_sig_out_3 	= new Double_t[n+1];			//output value for filter

                R = 0.002;
                if(highFilter == true)
                {

                    highpass(yy,n+1,dt,userFreq,R,y_sig_out_3);
                    setMaxVar();
                    setNewThres_sig3(n,y_sig_out_3);

                    Double_t maxsignal = adcdigi(n,xx,y_sig_out_3);

                    h1->Fill(maxsignal);
                    h1->Fit("gaus");
                    h1->Draw();
                }

                if(lowFilter == true)
                {

                    lowpass(yy,n+1,dt,userFreq,y_sig_out_3);
                    setMaxVar();
                    setNewThres_sig3(n,y_sig_out_3);

                    Double_t maxsignal = adcdigi(n,xx,y_sig_out_3);
                    cout << "max signal: " << maxsignal << endl;

                    h1->Fill(maxsignal);
                    h1->Fit("gaus");
                    h1->Draw();
                }

                c1->Update();

                /*=====================================================================*/
                /*=========================Creates histogram===========================*/

                if(did_sig_miss_thres(n,y_sig_out_1) == false && did_sig_miss_thres(n,y_sig_out_2) == false)
                {

                    Double_t time_sig1 = calcTime1(n,xx,y_sig_out_1);
                    Double_t time_sig2 = calcTime2(n,xx,y_sig_out_2);
                    time_final = time_sig1 - time_sig2; //compares the time difference(due to noise) of the signals after the filtering

                    cout << "Signal 1 crosses at " << time_sig1 << "us" << endl;
                    cout << "Signal 2 crosses at " << time_sig2 << "us" << endl;
                    cout << "----Both sigs crossed thres----"<< endl;
                }
                if(did_sig_miss_thres(n,y_sig_out_1) == true && did_sig_miss_thres(n,y_sig_out_2) == true)
                {
                    cout << "----Both sigs missed thres----"<< endl;
                }

                c1->cd(6);
                h2->Fill(time_final);
                h2->Fit("gaus");
                h2->Draw();
                c1->Update();

                delete[] xx;
                delete[] yy;
                delete[] yy2;


            }//end direct if
            n=0;
            if (display == 1)
            {
                if (doplot)
                    gets(s);
            }
            doplot = false;
        }//end create if

        //assigns values to initial array
        if(line.find("+") != found)
        {
            sscanf(&line[0],"%s %e %e",semn, &time, &amps);
            x[n]  = time;
            y[n]  = amps + noise->Gaus(0,0.234947);	//Thermal noise of 300ohm @ 300K @ 10GHz * 1.5 (1.5 for fun!)
            y2[n] = amps + noise->Gaus(0,0.234947);
            n++;
            if (amps != 0 ) doplot=true;
        }
    }//end while
}


#endif
