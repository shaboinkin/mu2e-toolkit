#ifndef __PLOTSIGNALS_SELECTION
#define __PLOTSIGNALS_SELECTION

/* plotsignals_selection.h

	-Header leads to other functions depending on the choices made by user
	Created By: Daniel Kulas

*/

#include "plotsignals_functions.h"
#include "plotsignals_graph.h"
#include "plotsignals_histo.h"
#include "plotsignals_both.h"
#include "plotsignals_rms.h"


Int_t   userFile,
        userFilter,
        userFreq;

Int_t   display = 1;

Double_t userBand;
float noiseCurrent;



void switchFilter()
{

    cout << "Would you like to view (1) Electron data, (2) protons data, or (3) gamma data?" << endl;
    cin  >> userFile;
    cout << "Would you like to apply (1) Highpass filter or (2) Lowpass filter?" << endl;
    cin  >> userFilter;

    switch(userFilter)
    {
    case 1:
        highFilter = true;
        lowFilter  = false;
        cout << "At what frequncy? 10 = 10 Mhz, 1 = 1Mhz, 0.1 = 10Khz, 0.01 = 1Khz, etc..." << endl;
        cin >> userFreq;
        setUserFixedThres(userFreq);
        break;
    case 2:
        lowFilter  = true;
        highFilter = false;
        cout << "At what frequncy? 10 = 10 Mhz, 1 = 1Mhz, 0.1 = 10Khz, 0.01 = 1Khz, etc..." << endl;
        cin >> userFreq;
        setUserFixedThres(userFreq);
        break;
    default:
        cout << "Please enter in 1) for Highpass filter or 2) for Lowpass filter." << endl;
        break;
    }
};

void drawGraph()
{

    Int_t display = 1;
    switchFilter();

    cout << "Please enter the bandwidth of your system to calculate the noise" << endl;
    cin  >> userBand;
    setNoiseCurr(userBand);

    switch(userFile)
    {
    case 1:
        cout << "Opening 'signalSr90_electrons.dat'....." << endl;
        theFile = "signalSr90_electrons.dat";
        plotsignals_graph(display, theFile, userFreq);
        break;
    case 2:
        cout << "Opening 'signalSr90_protons.dat'....." << endl;
        theFile = "signalSr90_protons.dat";
        plotsignals_graph(display, theFile, userFreq);
        break;
    case 3:
        cout << "Opening 'signalSr90_gammas.dat'...." << endl;
        theFile = "signalSr90_gammas.dat";
        cout << "herr" << endl;
        plotsignals_graph(display, theFile, userFreq);
        break;
    default:
        cout << "Please enter in 1) for electrons, 2) for protons, or 3) for gamma." << endl;
        break;
    }


};

void drawHisto()
{

    switchFilter();

    cout << "Please enter the bandwidth of your system to calculate the noise" << endl;
    cin  >> userBand;
    setNoiseCurr(userBand);

    switch(userFile)
    {
    case 1:
        cout << "Opening 'signalSr90_electrons.dat'....." << endl;
        theFile = "newoutput.dat";
        cout << theFile<< endl;
        plotsignals_histogram(theFile, userFreq);
        break;
    case 2:
        cout << "Opening 'protons.dat'....." << endl;
        theFile = "protons.dat";
        plotsignals_histogram(theFile, userFreq);
        break;
    case 3:
        cout << "Opening 'signalSr90_gammas.dat'...." << endl;
        theFile = "signalSr90_gammas.dat";
        plotsignals_histogram(theFile, userFreq);
        break;
    default:
        cout << "Please enter in 1) for electrons or 2) for protons" << endl;
        break;
    }
};

void drawBoth()
{

    switchFilter();

    cout << "Please enter the bandwidth of your system to calculate the noise" << endl;
    cin  >> userBand;
    setNoiseCurr(userBand);

    switch(userFile)
    {
    case 1:
        cout << "Opening 'signalSr90_electrons.dat'....." << endl;
        theFile = "signalSr90_electrons.dat";
        plotsignals_both(display, theFile, userFreq);
        break;
    case 2:
        cout << "Opening 'protons.dat'....." << endl;
        theFile = "protons.dat";
        plotsignals_both(display, theFile, userFreq);
        break;
    case 3:
        cout << "Opening 'signalSr90_gamma.dat'...." << endl;
        theFile = "signalSr90_gamma.dat";
        plotsignals_both(display, theFile, userFreq);
        break;
    default:
        cout << "Please enter in 1) for electrons or 2) for protons" << endl;
        break;
    }
};

#endif

