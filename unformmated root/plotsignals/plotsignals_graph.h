#ifndef __PLOTSIGNALS_GRAPH
#define __PLOTSIGNALS_GRAPH

/* plotsignals_graph.h

	-Header creates the graphs and outputs it one at a time
	Created By: Daniel Kulas

*/

#include "plotsignals_functions.h"
#include <fstream>

void plotsignals_graph(int display, const char *theFile, Double_t userFreq)
{
    //opens theFile for processing
    ifstream fp;
    fp.open(theFile);

    string line;
    char semn[1];

    unsigned int found = -1;
    int n = 0;

    float time;
    float amps;
    float x[100000];		//values from input file are stored in these arrays
    float y[100000];
    float y2[100000];
    float y3[100000];

    char *s = new char[1];
    bool doplot = false;
    float currentNoise;

    currentNoise = getNoiseCurr();
    cout << "currentNoise: " << currentNoise << endl;
    //int val = 1;
    //traces through input file line by line
    while (!fp.eof())
    {
        getline (fp,line);
        maxsig = 0;

        if (line.find("Created") != found && n > 0)
        {
            if (line.find("Direct signal") == found)
            {
                setFixedThres();

                /*=====================Initial Graph====================================*/
                cout << "" << endl;
                c1->Clear();
                c1->Divide(3,2);
                c1->cd(1);
                TGraph* gr = new TGraph(n,x,y);         //All objects with "T[name]" are root objects. See documentation online for more help
                gr->GetYaxis()->SetTitle("I(#muA)");
                gr->GetXaxis()->SetTitle("t(#mus)");
                gr->SetTitle("Initial Signal");

                if (display == 1)
                {
                    gr->Draw("AL");
                    c1->Update();
                }

                /*======================Initial Graph 2================================*/
                c1->cd(2);
                TGraph* gr2 = new TGraph(n,x,y2);
                gr2->GetYaxis()->SetTitle("I(#muA)");
                gr2->GetXaxis()->SetTitle("t(#mus)");
                gr2->SetTitle("Initial Signal 2");

                if(display == 1)
                {
                    gr2->Draw("AL");
                    c1->Update();
                }

                /*====================Signal with no noise===========================*/

                c1->cd(3);
                TGraph* gr3 = new TGraph(n,x,y3);
                gr3->GetYaxis()->SetTitle("I(#muA)");
                gr3->GetXaxis()->SetTitle("t(#mus)");
                gr3->SetTitle("Initial Signal, No noise");

                if(display == 1)
                {
                    gr3->Draw("AL");
                    c1->Update();
                }

                /*==========================Fill in arrays============================*/
                int firstbin = 3;
                Double_t *xx = new Double_t[n-firstbin];
                Double_t *yy = new Double_t[n-firstbin];
                Double_t *yy2 = new Double_t[n-firstbin];
                Double_t *yy3 = new Double_t[n-firstbin];


                for (int ih = 0; ih < n-firstbin; ih++)
                {
                    xx[ih] =x[ih+firstbin];							//Takes in values from the data file used and stores it into these arrays
                    yy[ih] =y[ih+firstbin];
                    yy2[ih] = y2[ih+firstbin];						//yy2 is the same input signal as yy, but with a different random noise
                    yy3[ih] = y3[ih+firstbin];
                }

                n = n-firstbin-1;

                /*=====================Draw bottom left graph=========================*/

                c1->cd(4);
                Double_t dt = x[n-1] - x[n-2];
                Double_t *y_sig_out_1 = new Double_t[n+1];
                Double_t R = 0.002;

                if(highFilter == true)
                {
                    highpass(yy,n+1,dt,userFreq,R,y_sig_out_1);
                    setNewThres_sig1(n,y_sig_out_1); //checks and sets a new threshold if needed
                    thresLine = new TLine(0,getNewThreshold_sig1(),0.25,getNewThreshold_sig1());

                    grhp1 = new TGraph( (n+1),xx,y_sig_out_1);
                    grhp1->GetYaxis()->SetTitle("I(#muA)");
                    grhp1->GetXaxis()->SetTitle("t(#mus)");
                    grhp1->SetTitle("Highpass on signal 1");
                    grhp1->GetX();
                }

                if(lowFilter == true)
                {
                    lowpass(yy,n+1,dt,userFreq,y_sig_out_1);
                    setMaxVar();
                    setNewThres_sig1(n,y_sig_out_1); //checks and sets a new threshold if needed
                    Double_t adcValue = adcdigi(n,xx,y_sig_out_1);
                    adcHitLine = new TLine(getadcHit(), 0, getadcHit(), -10);   //used to show where the ADC fires at
                    thresLine = new TLine(0,getNewThreshold_sig1(),0.25,getNewThreshold_sig1());

                    grhp1 = new TGraph( (n+1),xx,y_sig_out_1);
                    grhp1->GetYaxis()->SetTitle("I(#muA)");
                    grhp1->GetXaxis()->SetTitle("t(#mus)");
                    grhp1->SetTitle("Lowpass on signal 1");
                    grhp1->GetX();
                }
                

                if(display == 1)
                {
                    grhp1->Draw("AL");
                    adcHitLine->Draw();
                    thresLine->Draw();
                    baseLine->Draw();
                    c1->Update();
                }

                /*========================= Bottom right Graph=========================*/
                c1->cd(5);
                dt = x[n-1] - x[n-2];
                Double_t *y_sig_out_2 = new Double_t[n+1];
                R = 0.002;

                if(highFilter == true)
                {
                    highpass(yy2,n+1,dt,userFreq,R,y_sig_out_2);
                    setNewThres_sig2(n,y_sig_out_2); //checks and sets a new threshold if needed
                    thresLine2 = new TLine(0,getNewThreshold_sig2(),0.25,getNewThreshold_sig2());

                    grhp2 = new TGraph( (n+1),xx,y_sig_out_2);
                    grhp2->GetYaxis()->SetTitle("I(#muA)");
                    grhp2->GetXaxis()->SetTitle("t(#mus)");
                    grhp2->SetTitle("Highpass on signal 2");
                    grhp2->GetX();
                }

                if(lowFilter == true)
                {
                    lowpass(yy2,n+1,dt,userFreq,y_sig_out_2);
                    setMaxVar();    //stupid code. Just refreshes some values in the functions header file
                    setNewThres_sig2(n,y_sig_out_2); //checks and sets a new threshold if needed
                    thresLine2 = new TLine(0,getNewThreshold_sig2(),0.25,getNewThreshold_sig2());

                    grhp2 = new TGraph( (n+1),xx,y_sig_out_2);
                    grhp2->GetYaxis()->SetTitle("I(#muA)");
                    grhp2->GetXaxis()->SetTitle("t(#mus)");
                    grhp2->SetTitle("Lowpass on signal 2");
                    grhp2->GetX();
                }

                if(display == 1)
                {
                    grhp2->Draw("AL");
                    thresLine2->Draw();
                    baseLine->Draw();
                    c1->Update();
                }
                /*========================= Bottom right Graph=========================*/

                c1->cd(6);
                dt = x[n-1] - x[n-2];
                Double_t *y_sig_out_3 = new Double_t[n+1];
                R = 0.002;

                if(highFilter == true)
                {
                    highpass(yy3,n+1,dt,userFreq,R,y_sig_out_3);
                    setNewThres_sig3(n,y_sig_out_3); //checks and sets a new threshold if needed
                    thresLine = new TLine(0,getNewThreshold_sig3(),0.25,getNewThreshold_sig3());

                    grhp3 = new TGraph( (n+1),xx,y_sig_out_3);
                    grhp3->GetYaxis()->SetTitle("I(#muA)");
                    grhp3->GetXaxis()->SetTitle("t(#mus)");
                    grhp3->SetTitle("Highpass on signal ");
                    grhp3->GetX();
                }

                if(lowFilter == true)
                {
                    lowpass(yy3,n+1,dt,userFreq,y_sig_out_3);
                    setMaxVar();
                    setNewThres_sig3(n,y_sig_out_3); //checks and sets a new threshold if needed
                    thresLine = new TLine(0,getNewThreshold_sig3(),0.25,getNewThreshold_sig3());

                    grhp3 = new TGraph( (n+1),xx,y_sig_out_3);
                    grhp3->GetYaxis()->SetTitle("I(#muA)");
                    grhp3->GetXaxis()->SetTitle("t(#mus)");
                    grhp3->SetTitle("Lowpass on signal ");
                    grhp3->GetX();
                }

                if(display == 1)
                {
                    grhp3->Draw("AL");
                    thresLine->Draw();
                    baseLine->Draw();
                    c1->Update();
                }

                if(did_sig_miss_thres(n,y_sig_out_1) == false && did_sig_miss_thres(n,y_sig_out_2) == false)
                {
                    long double time_sig1 = calcTime1(n,xx,y_sig_out_1);
                    long double time_sig2 = calcTime2(n,xx,y_sig_out_2);

                    long double time_final = time_sig1 - time_sig2; //compares the time difference(due to noise) of the signals after the filtering

                    cout << "Signal 1 crosses at " << time_sig1*1000 << "ns" << endl;
                    cout << "Signal 2 crosses at " << time_sig2*1000 << "ns" << endl;
                    cout << "Difference in time: " << time_final*1000000<< "ps" << endl;
                    cout << "" << endl;
                    cout << "----Both signals crossed the threshold----"<< endl;

                }
                if(did_sig_miss_thres(n,y_sig_out_1) == true && did_sig_miss_thres(n,y_sig_out_2) == true)
                {
                    cout << "----Both signals missed the threshold----"<< endl;
                }

                setMaxVar();
                delete[] xx;
                delete[] yy;
                delete[] yy2;

            }//end direct if
            n=0;

            //this plots the graphs
            if (display == 1)
            {
                if (doplot)
                {
                    gets(s);
                    //  val--;
                }
            }
            doplot = false;

        }//end create if

        //assigns values to these array
        if(line.find("+") != found)
        {
            sscanf(&line[0],"%s %e %e",semn, &time, &amps);
            x[n]  = time;
            y[n]  = amps + noise->Gaus(0,currentNoise);            //Thermal noise of 300ohm @ 300K @ 1GHz
            y2[n] = amps + noise->Gaus(0,currentNoise);             
            y3[n] = amps;	//no noise
            n++;
            if (amps != 0 ) doplot=true;
        }
    }//end while
}//end plothighpass_graph

#endif
