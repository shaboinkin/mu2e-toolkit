#ifndef __PLOTSIGNALS_RMS
#define __PLOTSIGNALS_RMS

/* plotsignals_graph.h

	-Header creates the graphs and outputs it one at a time
	Created By: Daniel Kulas

*/

#include "plotsignals_functions.h"
#include <fstream>

void plotsignals_rms(int display, const char *theFile)
{

    ifstream fp;
    fp.open(theFile);

    string line;
    char semn[1];

    unsigned int found= -1;
    int n = 0;

    float time;
    float amps;
    float x[100000];		//values from input file are stored in these arrays
    float y[100000];

    char *s = new char[1];
    bool doplot = false;
    int C;

    Double_t calRMS;

    //traces through input file line by line
    while (!fp.eof())
    {
        getline (fp,line);
        cout << "got here" << endl;
        if (line.find("Created") != found && n > 0)
        {
            C = 1;
            if (line.find("Direct signal") == found)
            {

                /*==========================Fill in arrays============================*/
                int firstbin = 3;
                Double_t *xx = new Double_t[n-firstbin];
                Double_t *yy = new Double_t[n-firstbin];

                for (int ih = 0; ih < n-firstbin; ih++)
                {
                    xx[ih] =x[ih+firstbin];							//Takes in values from the data file used and stores it into these arrays
                    yy[ih] =y[ih+firstbin];

                }


                while(C != 0)
                {
                    calRMS = 0;
                    calRMS = calcRMS(n,yy);
                    cout << "RMS: " << calRMS << endl;
                    C = 0;
                }
                n = n-firstbin-1;



                delete[] xx;
                delete[] yy;

                calRMS = 0;

            }//end direct if
            n=0;

            if (display == 1)
            {
                if (doplot)
                    gets(s);
            }
            doplot = true;

        }//end create if

        //assigns values to initial array
        if(line.find("+") != found)
        {
            sscanf(&line[0],"%s %e %e",semn, &time, &amps);
            if((n % 10 == 0))
            {
                x[n]  = time;
                y[n]  = /*amps +*/ noise->Gaus(0,1);	////Thermal noise of 300ohm @ 300K @ 1GHz
                //ho   cout << "n: " << n << endl;
            }
            n++;

        }
    }//end while
}//end plothighpass_graph

#endif
