#ifndef __PLOTSIGNALS_HISTO
#define __PLOTSIGNALS_HISTO

/*  plotsignals_graph.h

	-Header gerenates data and outputs it to a histogram
	Created By: Daniel Kulas

*/

#include "plotsignals_functions.h"
#include <fstream>

void plotsignals_histogram(const char *theFile, Double_t userFreq)
{

    cout << "Generating histograms...please wait..." << endl;

    ifstream fp;
    fp.open(theFile);

    string line;
    char semn[1];
    unsigned int found = -1;
    int n = 0;

    float time;
    float amps;
    float x[10000];		//values from input file are stored in these arrays
    float y[10000];
    float y2[10000];

    bool doplot = false;
    int countMissedSig1 = 0;
    int countMissedSig2 = 0;

    int countDetectedSig1 = 0;
    int countDetectedSig2 = 0;

    float currentNoise;
    currentNoise = getNoiseCurr();


    //traces through input file line by line
    while (!fp.eof())
    {
        getline (fp,line);
        maxsig = 0;
        if (line.find("Created") != found && n > 0)
        {
            if (line.find("Direct signal") == found)   // && y[n-1] < 0){  //for some reason this really slows down the code
            {
                c1->Clear();
                c1->Divide(1,2);

                setFixedThres();

                /*==========================Fill in arrays============================*/
                int firstbin = 3;
                Double_t *xx = new Double_t[n-firstbin];
                Double_t *yy = new Double_t[n-firstbin];
                Double_t *yy2 = new Double_t[n-firstbin];
                for (int ih = 0; ih<n-firstbin; ih++)
                {
                    xx[ih] = x[ih+firstbin];				//Takes in values from the data file used and stores it into these arrays
                    yy[ih] = y[ih+firstbin];
                    yy2[ih] = y2[ih+firstbin];				//yy2 is the same input signal as yy, but with a different random noise
                }

                n = n-firstbin-1;

                /*======================Draw Max signal histogram=========================*/
                c1->cd(1);
                Double_t dt = x[n-1] - x[n-2];
                Double_t *y_sig_out_1 = new Double_t[n+1];
                Double_t R = 0.002;

                if(highFilter == true)
                {
                    highpass(yy,n+1,dt,userFreq,R,y_sig_out_1);
                    setMaxVar();
                    setNewThres_sig1(n,y_sig_out_1); //checks and sets a new threshold if needed

                    Double_t maxsignal = adcdigi(n,xx,y_sig_out_1);
                    h1->Fill(maxsignal);
                }

                if(lowFilter == true)
                {
                    lowpass(yy,n+1,dt,userFreq,y_sig_out_1);
                    setMaxVar();
                    setNewThres_sig1(n,y_sig_out_1); //checks and sets a new threshold if needed
                    Double_t maxsignal = findMax(n,y_sig_out_1);
                    cout << maxsignal << endl;
                    h1->Fill(maxsignal);
                    
                }

                h1->GetYaxis()->SetTitle("Occurrences");
                h1->GetXaxis()->SetTitle("I(#muA)");
                h1->Draw(); 


                /*=========================Draw delta t histogram=========================*/
                c1->cd(2);
                dt = x[n-1] - x[n-2];
                Double_t *y_sig_out_2 = new Double_t[n+1];
                R = 0.002;


                if(highFilter == true)
                {
                    highpass(yy2,n+1,dt,userFreq,R,y_sig_out_2);
                    setMaxVar();
                    setNewThres_sig2(n,y_sig_out_2);
                }

                if(lowFilter == true)
                {
                    lowpass(yy2,n+1,dt,userFreq,y_sig_out_2);
                    setMaxVar();
                    setNewThres_sig2(n,y_sig_out_2);
                }


                if(did_sig_miss_thres(n,y_sig_out_1) == false && did_sig_miss_thres(n,y_sig_out_2) == false)
                {
                    countDetectedSig1++;
                    countDetectedSig2++;
                    Double_t time_sig1 = calcTime1(n,xx,y_sig_out_1);
                    Double_t time_sig2 = calcTime2(n,xx,y_sig_out_2);

                    Double_t time_final = time_sig1 - time_sig2; //compares the time difference(due to noise) of the signals after the filtering
                    if(time_final > 0 || time_final < 0)
                    {
                        // cout << "delta t = " << time_final << endl;
                    }

                    h2->Fill(time_final);
                }
                if(did_sig_miss_thres(n,y_sig_out_1) == true && did_sig_miss_thres(n,y_sig_out_2) == true)
                {
                    countMissedSig1++;
                    countMissedSig2++;
                }

                h2->GetYaxis()->SetTitle("Occurrences");
                h2->GetXaxis()->SetTitle("t(#mus)");
                h2->Draw();
                // maxsignal = 0;

                delete[] xx;
                delete[] yy;
                delete[] yy2;
            }//end direct if

            n=0;
            doplot = false;

        }//end create if
        //assigns values to initial array

        if(line.find("+") != found)
        {
            sscanf(&line[0],"%s %e %e",semn, &time, &amps);
            x[n]  = time;
            y[n]  = amps + noise->Gaus(0,currentNoise);	////Thermal noise of 300ohm @ 300K @ user specified bandwidth
            y2[n] = amps + noise->Gaus(0,currentNoise);
            n++;
            if (amps != 0 ) doplot=true;
        }

    }//end while

    cout << "" << endl;
    cout << "Signal 1 missed: " << countMissedSig1 << " signals. " << endl;
    cout << countDetectedSig1 << " detected signals above threshold on signal 1" << endl;
    cout << "" << endl;
    cout << "Signal 2 missed: " << countMissedSig2 << " signals. " << endl;
    cout << countDetectedSig2 << " detected signals above threshold on signal 2" << endl;

// cout << "RMS: " << calRMS << endl;

    //h1->Fit("gaus");
    c1->Update();
    h2->Fit("gaus");
    c1->Update();

//uncomment to save histograms
    /*	TFile *maxSigFile = new TFile("[FILE NAME HERE].root", "RECREATE");
    	h1->Write();
    	maxSigFile->Close();

    	TFile *timeFile = new TFile("[FILE NAME HERE].root", "RECREATE");
    	h2->Write();
    	timeFile->Close();
    	*/

};
//end

#endif
