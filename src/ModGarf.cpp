/*************************** ModGarf.cpp ********************************
 *  					                                                *                        
 *                                                                      *                        
 *   This program changes the [name].in file that is passed to garfield	*				            
 *   to a value based on the distribution model of [signal] engergy     * 	                       
 *   levels.                                                            *                        
 *   For argv, passed the the file that contains the values of that     *                        
 *   model. The program will then output a modfied [name].in file       *                        
 *   Created by: Daniel Kulas    										*						
 *				 7/12/12     				                            *                        
 *               mu2e                                                   * 
 *              	                                                    *                         
 ************************************************************************/

#include <fstream>
#include <string>
#include <iostream>
#include <sstream>
#include <cstring>
#include <stdlib.h>

 using namespace std;

 int main(int argc, char* argv[])
 {
 	if(argc < 1)
 	{
 		cout << "Error: need to pass in arguments" << endl;
 		exit(0);
 	}

 	char energy_val[10];
 	strcpy(energy_val, argv[1]);

 	istringstream get_energy(energy_val);
 	double energy;
 	get_energy >> energy;		//get the char* and make it a double

 	unsigned int notfound = -1;

 	//if you need to rebuild for anyreason, uncomment the stuff you need

 	string elec = "track -{xpos} {ypos} 0 {xpos} {ypos} 0 electron energy 100 MeV";
 	//string gamm = "track -{xpos} {ypos} 0 {xpos} {ypos} 0 gamma energy 6 KeV";
 	//string prot = "track -{xpos} {ypos} 0 {xpos} {ypos} 0 proton energy 10 MeV";
 	string line_in;
 	string line_val;
 
 	ifstream infile;
 	ofstream output("signalrndSr90_electron.in");
 	infile.open("signalrndSr90_elec.in");

 	//GAMMAS
 	//ofstream output("signalrndSr90_gamma.in");
 	//infile.open("signalrndSr90_gamm.in");
 	//PROTONS
 	//ofstream output("signalrndSr90_proton.in");
 	//infile.open("signalrndSr90_prot.in");
 	
 	while(!infile.eof())
 	{
 		getline(infile, line_in);

 		if(line_in.find(elec) != notfound)
 		{			
 			output << "track -{xpos} {ypos} 0 {xpos} {ypos} 0 electron energy " << energy << " MeV ..." << endl;
 			
 			//GAMMAS
 			//output << "track -{xpos} {ypos} 0 {xpos} {ypos} 0 gamma energy " << energy << " KeV ..." << endl;
 			
 			//PROTONS
 			//output << "track -{xpos} {ypos} 0 {xpos} {ypos} 0 proton energy " << energy << " MeV ..." << endl;
 		}

 		if(line_in.find(elec) == notfound)
 		{
 			output << line_in << endl;
 		}
 	}

 	infile.close();
 	return 0;
}