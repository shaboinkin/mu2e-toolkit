/*************************** CombineOutputs.cpp *************************
 *  					                                                *                        
 *                                                                      *                        
 * Based on the same code to combine the outputs from the spice output  *	                       
 * Input the .dat file from the script and combine them all together    *                                               
 * 																		*
 * The .in file only produces 1 signal at some energy level				*
 * Combine these .dat files together so the rest of the script can work *
 *																		*                        
 *   Created by: Daniel Kulas    										*						
 *				 7/9/12     				                            *                        
 *               mu2e                                                   * 
 *              	                                                    *                         
 ************************************************************************/

#include <fstream>
#include <string>
#include <iostream>
#include <sstream>
#include <cstring>
#include <stdlib.h>
				
using namespace std;

int main(int argc, char* argv[])
{
	if(argc < 1)
	{
		cout << "Error: No file was passed in...abort" << endl;
		exit(1);
	}

	string line;
	char theFile[100];
	strcpy(theFile, argv[1]);
	ifstream input;
	//If need to rebuild the file to include a different .dat file, just change the name below to what the .dat file is
	ofstream output("signalSr90_electron_new.dat", fstream::in | fstream::out | fstream::app);	//scary voodoo black magic happens here

	input.open(theFile); 

	while(!input.eof())
	{
		getline(input, line);
		output << line << endl;
	}

	input.close();
	output.close();
}
