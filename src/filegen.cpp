// Simple program to output different amounts of MeV to be passed to garfield
// If you need different energy levels, just modify "energy" and the loop
// Created By: Daniel Kulas
//			   7/11/12 

#include <fstream>
#include <string>
#include <iostream>
#include <sstream>


using namespace std;

int main()
{
	ofstream output("eV.txt");
	stringstream ss;

	int energy = 1;	//in MeV
	string ev = " MeV";
	string out; 
	for(int i = 0; i < 105; i++)
	{
		ss << energy << ev << endl;
		out = ss.str();
		energy++;
	}

	output << out << endl;
	return 0;
}