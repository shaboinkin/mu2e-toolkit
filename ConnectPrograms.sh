			#################################################
		   #################################################
          ##############                  #################
         #############  ConnectPrograms.sh  ##############
        #######        Script to connect    	  #######
       #######   Garfield, ngspice, and Root     #######
      #######                                   #######
     #######      Created by: Daniel Kulas     #######
    #######              6/28/12              #######
   ###############    --mu2e--     #################
  #################################################
 #################################################
#################################################
################set directories##################
mPATH=$HOME/mu2e_toolkit_3.0
singlePATH=$mPATH/Singleprocess
mutliPATH=$mPATH/Multiprocess
#################################################
################singlePATH dirs##################
rPATH=$mPATH/root/
stoPATH=$rPATH/data_storage/

gPATH=$singlePATH/garfield
fPATH=$singlePATH/formatting

gPATH_FIXED=$gPATH/Fixed_Sized_Signals/

gFORMATTING=$fPATH/gFiles/Storage/
sFORMATTING=$fPATH/sFiles/Storage/

sPATH=$singlePATH/spice/Preamp
sPATH_ELEC=$sPATH/electrons/
sPATH_PROT=$sPATH/protons/
sPATH_GAMM=$sPATH/gammas/
#################################################
#########fixed signal files######################
GARF_FILE_E_L="signalSr90_electron_large.dat"
GARF_FILE_E_S="signalSr90_electron_small.dat"

GARF_FILE_G_L="signalSr90_gamma_large.dat"
GARF_FILE_G_S="signalSr90_gamma_small.dat"

GARF_FILE_P_L="signalSr90_proton_large.dat"
GARF_FILE_P_S="signalSr90_proton_small.dat"

GARF_FILE_E_L_2="signalSr90_electron_large2.dat"
GARF_FILE_E_S_2="signalSr90_electron_small2.dat"

GARF_FILE_G_L_2="signalSr90_gamma_large2.dat"
GARF_FILE_G_S_2="signalSr90_gamma_small2.dat"

GARF_FILE_P_L_2="signalSr90_proton_large2.dat"
GARF_FILE_P_S_2="signalSr90_proton_small2.dat"
#################################################
################ngspice stuff####################
SIGDAT="signal.dat"
OUT="output.dat"
#################################################
################multiPATH dirs###################
P0=$mutliPATH/P0/
P0SPICE_ELEC=$P0/spice/Preamp/electrons/
P0SPICE_GAMM=$P0/spice/Preamp/gammas/
P0SPICE_PROT=$P0/spice/Preamp/protons/

P1=$mutliPATH/P1/
P1SPICE_ELEC=$P1/spice/Preamp/electrons/
P1SPICE_GAMM=$P1/spice/Preamp/gammas/
P1SPICE_PROT=$P1/spice/Preamp/protons/

P2=$mutliPATH/P2/
P2SPICE_ELEC=$P2/spice/Preamp/electrons/
P2SPICE_GAMM=$P2/spice/Preamp/gammas/
P2SPICE_PROT=$P2/spice/Preamp/protons/

P3=$mutliPATH/P3/
P3SPICE_ELEC=$P3/spice/Preamp/electrons/
P3SPICE_GAMM=$P3/spice/Preamp/gammas/
P3SPICE_PROT=$P3/spice/Preamp/protons/

P4=$mutliPATH/P4/
P4SPICE_ELEC=$P4/spice/Preamp/electrons/
P4SPICE_GAMM=$P4/spice/Preamp/gammas/
P4SPICE_PROT=$P4/spice/Preamp/protons/

P5=$mutliPATH/P5/
P5SPICE_ELEC=$P5/spice/Preamp/electrons/
P5SPICE_GAMM=$P5/spice/Preamp/gammas/
P5SPICE_PROT=$P5/spice/Preamp/protons/

P6=$mutliPATH/P6/
P6SPICE_ELEC=$P6/spice/Preamp/electrons/
P6SPICE_GAMM=$P6/spice/Preamp/gammas/
P6SPICE_PROT=$P6/spice/Preamp/protons/

P7=$mutliPATH/P7/
P7SPICE_ELEC=$P7/spice/Preamp/electrons/
P7SPICE_GAMM=$P7/spice/Preamp/gammas/
P7SPICE_PROT=$P7/spice/Preamp/protons/

DATSTORAGE=$mutliPATH/datStorage/

OUT0="newoutput_P0.dat"
OUT1="newoutput_P1.dat"
OUT2="newoutput_P2.dat"
OUT3="newoutput_P3.dat"
OUT4="newoutput_P4.dat"
OUT5="newoutput_P5.dat"
OUT6="newoutput_P6.dat"
OUT7="newoutput_P7.dat"
#################################################
############remove old dat files#################

find $gPATH -name '*.dat' -exec rm {} \;
find $sFORMATTING -name '*.dat' -exec rm {} \;
find $gPATH -name '*.dat' -exec rm {} \; 
find $sPATH_ELEC -name '*.dat' -exec rm {} \;
find $sPATH_GAMM -name '*.dat' -exec rm {} \; 
find $sPATH_PROT -name '*.dat' -exec rm {} \; 
find $stoPATH -name '*.dat' -exec rm {} \;
find $rPATH -name '*.dat' -exec rm {} \;

#################################################
################shitty code######################
FAIL=0
#	There has to be a better way of doing this.... 


#	chooseProcessAmount()
#
#	This function starts 'x' processes based on the users choise of "core_selection"
#	The '( )' between each instruction is a werid solution to a problem I was having with, long story short, shit not working
#
#	If interested, read this for some understanding on the '( )'  http://cygwin.com/ml/cygwin/1998-04/msg00413.html
function chooseProcessAmount() {
	case "$core_selection" in
	1)
		$P0./RunGarf_P0.sh $iterations $sig_selection
		wait
		COMBINE=$?
		echo "combine: "$COMBINE
		;;
	2)
		($P0./RunGarf_P0.sh $iterations $sig_selection &)
		$P1./RunGarf_P1.sh $iterations $sig_selection & 
		
		for job in `jobs -p`
		do
			wait $job || let "FAIL+=1"
		done

		if [ "$FAIL" == "0" ]; then
			echo "No errors with the signals. Continuing..."
		else
			echo "FAIL! ($FAIL)"
			echo "There were problems with the signals. Aborting..."
			exit
			bash
		fi
		;;
	3)
		($P0./RunGarf_P0.sh $iterations $sig_selection &)
		($P1./RunGarf_P1.sh $iterations $sig_selection &)
		$P2./RunGarf_P2.sh $iterations $sig_selection &
		wait
		;;
	4)
		($P0./RunGarf_P0.sh $iterations $sig_selection &)
		($P1./RunGarf_P1.sh $iterations $sig_selection &)
		($P2./RunGarf_P2.sh $iterations $sig_selection &)
		$P3./RunGarf_P3.sh $iterations $sig_selection &
		wait
		;;
	5)
		($P0./RunGarf_P0.sh $iterations $sig_selection &)
		($P1./RunGarf_P1.sh $iterations $sig_selection &)
		($P2./RunGarf_P2.sh $iterations $sig_selection &)
		($P3./RunGarf_P3.sh $iterations $sig_selection &)
		$P4./RunGarf_P4.sh $iterations $sig_selection &
		wait
		;;
	6)
		($P0./RunGarf_P0.sh $iterations $sig_selection &)
		($P1./RunGarf_P1.sh $iterations $sig_selection &)
		($P2./RunGarf_P2.sh $iterations $sig_selection &)
		($P3./RunGarf_P3.sh $iterations $sig_selection &)
		($P4./RunGarf_P4.sh $iterations $sig_selection &)
		$P5./RunGarf_P5.sh $iterations $sig_selection &
		wait
		;;
	7)
		($P0./RunGarf_P0.sh $iterations $sig_selection &)
		($P1./RunGarf_P1.sh $iterations $sig_selection &)
		($P2./RunGarf_P2.sh $iterations $sig_selection &)
		($P3./RunGarf_P3.sh $iterations $sig_selection &)
		($P4./RunGarf_P4.sh $iterations $sig_selection &)
		($P5./RunGarf_P5.sh $iterations $sig_selection &)
		$P6./RunGarf_P6.sh $iterations $sig_selection &
		wait		   
	   	;;
	8)
		($P0./RunGarf_P0.sh $iterations $sig_selection &)
		($P1./RunGarf_P1.sh $iterations $sig_selection &)
		($P2./RunGarf_P2.sh $iterations $sig_selection &)
		($P3./RunGarf_P3.sh $iterations $sig_selection &)
		($P4./RunGarf_P4.sh $iterations $sig_selection &)
		($P5./RunGarf_P5.sh $iterations $sig_selection &)
		($P6./RunGarf_P6.sh $iterations $sig_selection &)
	   	$P7./RunGarf_P7.sh $iterations $sig_selection &
	    wait
	    ;;
	*)
		#default action is to run 1 core
		echo "Inputted a core amount I'm not familar with."
		echo "Continuing with default of 1 core"
		$P0./RunGarf_P0.sh $iterations $sig_selection		
	esac
}
#	combineFiles()
#	
#	After running through your processes, again, based on your core selection, will automatically move your files to be formatted 
#	for ROOT
#
#	This function is extremely repetive. If you know of a better way to deal with this, fix it.
#
function combineFiles() {
	case "$core_selection" in
		1)
			
			if [ "$sig_selection" == "E" -o "$sig_selection" == "e" -a "$COMBINE" == 0 ]; then 	   		
				cd $P0SPICE_ELEC
				mv newoutput.dat $OUT0
				mv $OUT0 newoutput.dat
			 	mv newoutput.dat $rPATH

			elif [ "$sig_selection" == "G" -o "$sig_selection" == "g" -a "$COMBINE" == 0 ]; then
			    cd $P0SPICE_GAMM
			    mv newoutput.dat $OUT0
			    mv $OUT0 newoutput.dat
			    mv newoutput.dat $rPATH

			elif [ "$sig_selection" == "P" -o "$sig_selection" == "p" -a "$COMBINE" == 0 ]; then
			    cd $P0SPICE_PROT
			    mv newoutput.dat $OUT0
			    mv $OUT0 newoutput.dat
			    mv newoutput.dat $rPATH

			elif [ "$COMBINE" != 0 ]; then
				echo "Garfield produced no signals"
				exit
			else
				echo "Random error message"
				echo "..."
				echo "What could this mean? WHO KNOWS!! It's anyones guess!"
				echo "Maybe it's a new feature!?! Maybe your system developed a mind of it's own!!"
				echo "It could mean anything! How exciting!"
			fi	
			;;

		2)
			if [ "$sig_selection" == "E" -o "$sig_selection" == "e" -a "$COMBINE_P0" != "-1" -a "$COMBINE_P1" != "-1" ]; then 	   	

				#Goes to process 0's spice folder for electrons
				cd $P0SPICE_ELEC							
				#renames newoutput.dat to newoutput1.dat
				mv newoutput.dat $OUT0 >/dev/null 2>&1
				#moves newoutput1.dat to the storage folder for roo
				mv $OUT0 $stoPATH >/dev/null 2>&1
				#go to that folder
				cd $stoPATH
				#combine newoutput1.dat to newoutput.dat (that file is generated automattically by the program)
				./CombineFinalOutputs $OUT0
				wait
				#Wash, rinse, repeat
				cd $P1SPICE_ELEC
				mv newoutput.dat $OUT1 >/dev/null 2>&1
				mv $OUT1 $stoPATH >/dev/null 2>&1
				cd $stoPATH
				./CombineFinalOutputs $OUT1
				wait
				mv newoutput.dat $rPATH 

			elif [ "$sig_selection" == "G" -o "$sig_selection" == "g" -a "$COMBINE_P0" != "-1" -a "$COMBINE_P1" != "-1" ]; then

				cd $P0SPICE_GAMM
				mv newoutput.dat $OUT0 	>/dev/null 2>&1
				mv $OUT0 $stoPATH 		>/dev/null 2>&1
				cd $stoPATH
				./CombineFinalOutputs $OUT0
				wait
				cd $P1SPICE_GAMM
				mv newoutput.dat $OUT1 	>/dev/null 2>&1
				mv $OUT1 $stoPATH 		>/dev/null 2>&1
				cd $stoPATH
				./CombineFinalOutputs $OUT1
				wait
				mv newoutput.dat $rPATH 

			elif [ "$sig_selection" == "P" -o "$sig_selection" == "p" -a "$COMBINE_P0" != "-1" -a "$COMBINE_P1" != "-1" ]; then

				cd $P0SPICE_PROT
				mv newoutput.dat $OUT0 	>/dev/null 2>&1
				mv $OUT0 $stoPATH 		>/dev/null 2>&1
				cd $stoPATH
				./CombineFinalOutputs $OUT0
				wait
				cd $P1SPICE_PROT
				mv newoutput.dat $OUT1 	>/dev/null 2>&1
				mv $OUT1 $stoPATH		>/dev/null 2>&1
				cd $stoPATH
				./CombineFinalOutputs $OUT1
				wait
				mv newoutput.dat $rPATH 

			elif [ "$COMBINE_P0" == "-1" -o "$COMBINE_P1" == "-1" ]; then
				echo "Cannot combine signals...abort"
				exit

			else
				echo "You shouldn't have seen this. This is super secret text...aborting"
				exit
			fi
			;;
		3)
			if [ "$sig_selection" == "E" -o "$sig_selection" == "e" ]; then 	   	

				cd $P0SPICE_ELEC
				mv newoutput.dat $OUT0 	>/dev/null 2>&1
				mv $OUT0 $stoPATH 		>/dev/null 2>&1
				cd $stoPATH
				./CombineFinalOutputs $OUT0
				wait
				cd $P1SPICE_ELEC
				mv newoutput.dat $OUT1 	>/dev/null 2>&1
				mv $OUT1 $stoPATH 		>/dev/null 2>&1
				cd $stoPATH
				./CombineFinalOutputs $OUT1
				wait
				cd $P2SPICE_ELEC
				mv newoutput.dat $OUT2 	>/dev/null 2>&1
				mv $OUT2 $stoPATH 		>/dev/null 2>&1
				cd $stoPATH
				./CombineFinalOutputs $OUT2
				wait
				mv newoutput.dat $rPATH 

			elif [ "$sig_selection" == "G" -o "$sig_selection" == "g" ]; then

				cd $P0SPICE_GAMM
				mv newoutput.dat $OUT0 >/dev/null 2>&1
				mv $OUT0 $stoPATH >/dev/null 2>&1
				cd $stoPATH
				./CombineFinalOutputs $OUT0
				wait
				cd $P1SPICE_GAMM
				mv newoutput.dat $OUT1 >/dev/null 2>&1
				mv $OUT1 $stoPATH >/dev/null 2>&1
				cd $stoPATH
				./CombineFinalOutputs $OUT1
				wait
				cd $P2SPICE_GAMM
				mv newoutput.dat $OUT2 	>/dev/null 2>&1
				mv $OUT2 $stoPATH 		>/dev/null 2>&1
				cd $stoPATH
				./CombineFinalOutputs $OUT2
				wait
				mv newoutput.dat $rPATH 

			elif [ "$sig_selection" == "P" -o "$sig_selection" == "p" ]; then

				cd $P0SPICE_PROT
				mv newoutput.dat $OUT0 >/dev/null 2>&1
				mv $OUT0 $stoPATH >/dev/null 2>&1
				cd $stoPATH
				./CombineFinalOutputs $OUT0
				wait
				cd $P1SPICE_PROT
				mv newoutput.dat $OUT1 >/dev/null 2>&1
				mv $OUT1 $stoPATH >/dev/null 2>&1
				cd $stoPATH
				./CombineFinalOutputs $OUT1
				wait
				cd $P2SPICE_PROT
				mv newoutput.dat $OUT2 	>/dev/null 2>&1
				mv $OUT2 $stoPATH 		>/dev/null 2>&1
				cd $stoPATH
				./CombineFinalOutputs $OUT2
				wait
				mv newoutput.dat $rPATH 

			else
				echo "You shouldn't have seen this. This is super secret text...aborting"
				exit
			fi   	
			;;
		4)
			if [ "$sig_selection" == "E" -o "$sig_selection" == "e" ]; then 	   	

				cd $P0SPICE_ELEC
				mv newoutput.dat $OUT0 	>/dev/null 2>&1
				mv $OUT0 $stoPATH 		>/dev/null 2>&1
				cd $stoPATH
				./CombineFinalOutputs $OUT0
				wait
				cd $P1SPICE_ELEC
				mv newoutput.dat $OUT1 	>/dev/null 2>&1
				mv $OUT1 $stoPATH 		>/dev/null 2>&1
				cd $stoPATH
				./CombineFinalOutputs $OUT1
				wait
				cd $P2SPICE_ELEC
				mv newoutput.dat $OUT2 	>/dev/null 2>&1
				mv $OUT2 $stoPATH 		>/dev/null 2>&1
				cd $stoPATH
				./CombineFinalOutputs $OUT2
				wait
				cd $P3SPICE_ELEC
				mv newoutput.dat $OUT3 	>/dev/null 2>&1
				mv $OUT3 $stoPATH 		>/dev/null 2>&1
				cd $stoPATH
				./CombineFinalOutputs $OUT3
				wait
				mv newoutput.dat $rPATH 

			elif [ "$sig_selection" == "G" -o "$sig_selection" == "g" ]; then

				cd $P0SPICE_GAMM
				mv newoutput.dat $OUT0 >/dev/null 2>&1
				mv $OUT0 $stoPATH >/dev/null 2>&1
				cd $stoPATH
				./CombineFinalOutputs $OUT0
				wait
				cd $P1SPICE_GAMM
				mv newoutput.dat $OUT1 >/dev/null 2>&1
				mv $OUT1 $stoPATH >/dev/null 2>&1
				cd $stoPATH
				./CombineFinalOutputs $OUT1
				wait
				cd $P2SPICE_GAMM
				mv newoutput.dat $OUT2 	>/dev/null 2>&1
				mv $OUT2 $stoPATH 		>/dev/null 2>&1
				cd $stoPATH
				./CombineFinalOutputs $OUT2
				wait
				cd $P3SPICE_GAMM
				mv newoutput.dat $OUT3 	>/dev/null 2>&1
				mv $OUT3 $stoPATH 		>/dev/null 2>&1
				cd $stoPATH
				./CombineFinalOutputs $OUT3
				wait	
				mv newoutput.dat $rPATH 

			elif [ "$sig_selection" == "P" -o "$sig_selection" == "p" ]; then

				cd $P0SPICE_PROT
				mv newoutput.dat $OUT0 >/dev/null 2>&1
				mv $OUT0 $stoPATH >/dev/null 2>&1
				cd $stoPATH
				./CombineFinalOutputs $OUT0
				wait
				cd $P1SPICE_PROT
				mv newoutput.dat $OUT1 >/dev/null 2>&1
				mv $OUT1 $stoPATH >/dev/null 2>&1
				cd $stoPATH
				./CombineFinalOutputs $OUT1
				wait
				cd $P2SPICE_PROT
				mv newoutput.dat $OUT2 	>/dev/null 2>&1
				mv $OUT2 $stoPATH 		>/dev/null 2>&1
				cd $stoPATH
				./CombineFinalOutputs $OUT2
				wait
				cd $P3SPICE_PROT
				mv newoutput.dat $OUT3 	>/dev/null 2>&1
				mv $OUT3 $stoPATH 		>/dev/null 2>&1
				cd $stoPATH
				./CombineFinalOutputs $OUT3
				wait 		   
				mv newoutput.dat $rPATH 

			else
				echo "You shouldn't have seen this. This is super secret text...aborting"
				exit
			fi		   
			;;
		5)
			if [ "$sig_selection" == "E" -o "$sig_selection" == "e" ]; then 	   	
				cd $P0SPICE_ELEC
				mv newoutput.dat $OUT0 	>/dev/null 2>&1
				mv $OUT0 $stoPATH 		>/dev/null 2>&1
				cd $stoPATH
				./CombineFinalOutputs $OUT0
				wait
				cd $P1SPICE_ELEC
				mv newoutput.dat $OUT1 	>/dev/null 2>&1
				mv $OUT1 $stoPATH 		>/dev/null 2>&1
				cd $stoPATH
				./CombineFinalOutputs $OUT1
				wait
				cd $P2SPICE_ELEC
				mv newoutput.dat $OUT2 	>/dev/null 2>&1
				mv $OUT2 $stoPATH 		>/dev/null 2>&1
				cd $stoPATH
				./CombineFinalOutputs $OUT2
				wait
				cd $P3SPICE_ELEC
				mv newoutput.dat $OUT3 	>/dev/null 2>&1
				mv $OUT3 $stoPATH 		>/dev/null 2>&1
				cd $stoPATH
				./CombineFinalOutputs $OUT3
				wait
				cd $P4SPICE_ELEC
				mv newoutput.dat $OUT4 	>/dev/null 2>&1
				mv $OUT4 $stoPATH 		>/dev/null 2>&1
				cd $stoPATH
				./CombineFinalOutputs $OUT4
				wait
				mv newoutput.dat $rPATH 

			elif [ "$sig_selection" == "G" -o "$sig_selection" == "g" ]; then

				cd $P0SPICE_GAMM
				mv newoutput.dat $OUT0 >/dev/null 2>&1
				mv $OUT0 $stoPATH >/dev/null 2>&1
				cd $stoPATH
				./CombineFinalOutputs $OUT0
				wait
				cd $P1SPICE_GAMM
				mv newoutput.dat $OUT1 >/dev/null 2>&1
				mv $OUT1 $stoPATH >/dev/null 2>&1
				cd $stoPATH
				./CombineFinalOutputs $OUT1
				wait
				cd $P2SPICE_GAMM
				mv newoutput.dat $OUT2 	>/dev/null 2>&1
				mv $OUT2 $stoPATH 		>/dev/null 2>&1
				cd $stoPATH
				./CombineFinalOutputs $OUT2
				wait
				cd $P3SPICE_GAMM
				mv newoutput.dat $OUT3 	>/dev/null 2>&1
				mv $OUT3 $stoPATH 		>/dev/null 2>&1
				cd $stoPATH
				./CombineFinalOutputs $OUT3
				wait
				cd $P4SPICE_GAMM
				mv newoutput.dat $OUT4 	>/dev/null 2>&1
				mv $OUT4 $stoPATH 		>/dev/null 2>&1
				cd $stoPATH
				./CombineFinalOutputs $OUT4
				wait
				mv newoutput.dat $rPATH 

			elif [ "$sig_selection" == "P" -o "$sig_selection" == "p" ]; then

				cd $P0SPICE_PROT
				mv newoutput.dat $OUT0 >/dev/null 2>&1
				mv $OUT0 $stoPATH >/dev/null 2>&1
				cd $stoPATH
				./CombineFinalOutputs $OUT0
				wait
				cd $P1SPICE_PROT
				mv newoutput.dat $OUT1 >/dev/null 2>&1
				mv $OUT1 $stoPATH >/dev/null 2>&1
				cd $stoPATH
				./CombineFinalOutputs $OUT1
				wait
				cd $P2SPICE_PROT
				mv newoutput.dat $OUT2 	>/dev/null 2>&1
				mv $OUT2 $stoPATH 		>/dev/null 2>&1
				cd $stoPATH
				./CombineFinalOutputs $OUT2
				wait
				cd $P3SPICE_PROT
				mv newoutput.dat $OUT3 	>/dev/null 2>&1
				mv $OUT3 $stoPATH 		>/dev/null 2>&1
				cd $stoPATH
				./CombineFinalOutputs $OUT3
				wait
				cd $P4SPICE_PROT
				mv newoutput.dat $OUT4 	>/dev/null 2>&1
				mv $OUT4 $stoPATH 		>/dev/null 2>&1
				cd $stoPATH
				./CombineFinalOutputs $OUT4
				wait		   		   		   
				mv newoutput.dat $rPATH 

			else
				echo "You shouldn't have seen this. This is super secret text...aborting"
				exit
			fi			   
		  	;;
		6)	
		   	if [ "$sig_selection" == "E" -o "$sig_selection" == "e" ]; then 	   	

			   cd $P0SPICE_ELEC
			   mv newoutput.dat $OUT0 	>/dev/null 2>&1
			   mv $OUT0 $stoPATH 		>/dev/null 2>&1
			   cd $stoPATH
			   ./CombineFinalOutputs $OUT0
			   wait
			   cd $P1SPICE_ELEC
			   mv newoutput.dat $OUT1 	>/dev/null 2>&1
			   mv $OUT1 $stoPATH 		>/dev/null 2>&1
			   cd $stoPATH
			   ./CombineFinalOutputs $OUT1
			   wait
			   cd $P2SPICE_ELEC
			   mv newoutput.dat $OUT2 	>/dev/null 2>&1
			   mv $OUT2 $stoPATH 		>/dev/null 2>&1
			   cd $stoPATH
			   ./CombineFinalOutputs $OUT2
			   wait
			   cd $P3SPICE_ELEC
			   mv newoutput.dat $OUT3 	>/dev/null 2>&1
			   mv $OUT3 $stoPATH 		>/dev/null 2>&1
			   cd $stoPATH
			   ./CombineFinalOutputs $OUT3
			   wait
			   cd $P4SPICE_ELEC
			   mv newoutput.dat $OUT4 	>/dev/null 2>&1
			   mv $OUT4 $stoPATH 		>/dev/null 2>&1
			   cd $stoPATH
			   ./CombineFinalOutputs $OUT4
			   wait
			   cd $P5SPICE_ELEC
			   mv newoutput.dat $OUT5 	>/dev/null 2>&1
			   mv $OUT5 $stoPATH 		>/dev/null 2>&1
			   cd $stoPATH
			   ./CombineFinalOutputs $OUT5
			   wait
			   mv newoutput.dat $rPATH 

			elif [ "$sig_selection" == "G" -o "$sig_selection" == "g" ]; then

			   cd $P0SPICE_GAMM
			   mv newoutput.dat $OUT0 >/dev/null 2>&1
			   mv $OUT0 $stoPATH >/dev/null 2>&1
			   cd $stoPATH
			   ./CombineFinalOutputs $OUT0
			   wait
			   cd $P1SPICE_GAMM
			   mv newoutput.dat $OUT1 >/dev/null 2>&1
			   mv $OUT1 $stoPATH >/dev/null 2>&1
			   cd $stoPATH
			   ./CombineFinalOutputs $OUT1
			   wait
			   cd $P2SPICE_GAMM
			   mv newoutput.dat $OUT2 	>/dev/null 2>&1
			   mv $OUT2 $stoPATH 		>/dev/null 2>&1
			   cd $stoPATH
			   ./CombineFinalOutputs $OUT2
			   wait
			   cd $P3SPICE_GAMM
			   mv newoutput.dat $OUT3 	>/dev/null 2>&1
			   mv $OUT3 $stoPATH 		>/dev/null 2>&1
			   cd $stoPATH
			   ./CombineFinalOutputs $OUT3
			   wait
			   cd $P4SPICE_GAMM
			   mv newoutput.dat $OUT4 	>/dev/null 2>&1
			   mv $OUT4 $stoPATH 		>/dev/null 2>&1
			   cd $stoPATH
			   ./CombineFinalOutputs $OUT4
			   wait
			   cd $P5SPICE_GAMM
			   mv newoutput.dat $OUT5 	>/dev/null 2>&1
			   mv $OUT5 $stoPATH 		>/dev/null 2>&1
			   cd $stoPATH
			   ./CombineFinalOutputs $OUT5
			   wait
			   mv newoutput.dat $rPATH 

			elif [ "$sig_selection" == "P" -o "$sig_selection" == "p" ]; then
			   echo "####################"
			   echo "IN PROTONS"
			   echo "###################"
			   cd $P0SPICE_PROT
			   mv newoutput.dat $OUT0 >/dev/null 2>&1
			   mv $OUT0 $stoPATH >/dev/null 2>&1
			   cd $stoPATH
			   ./CombineFinalOutputs $OUT0
			   wait
			   cd $P1SPICE_PROT
			   mv newoutput.dat $OUT1 >/dev/null 2>&1
			   mv $OUT1 $stoPATH >/dev/null 2>&1
			   cd $stoPATH
			   ./CombineFinalOutputs $OUT1
			   wait
			   cd $P2SPICE_PROT
			   mv newoutput.dat $OUT2 	>/dev/null 2>&1
			   mv $OUT2 $stoPATH 		>/dev/null 2>&1
			   cd $stoPATH
			   ./CombineFinalOutputs $OUT2
			   wait
			   cd $P3SPICE_PROT
			   mv newoutput.dat $OUT3 	>/dev/null 2>&1
			   mv $OUT3 $stoPATH 		>/dev/null 2>&1
			   cd $stoPATH
			   ./CombineFinalOutputs $OUT3
			   wait
			   cd $P4SPICE_PROT
			   mv newoutput.dat $OUT4 	>/dev/null 2>&1
			   mv $OUT4 $stoPATH 		>/dev/null 2>&1
			   cd $stoPATH
			   ./CombineFinalOutputs $OUT4
			   wait
			   cd $P5SPICE_PROT
			   mv newoutput.dat $OUT5 	>/dev/null 2>&1
			   mv $OUT5 $stoPATH 		>/dev/null 2>&1
			   cd $stoPATH
			   ./CombineFinalOutputs $OUT5
			   wait	   		   		   
			   mv newoutput.dat $rPATH 

			else
				echo "You shouldn't have seen this. This is super secret text...aborting"
				exit
			fi		
		   ;;
		7)
			if [ "$sig_selection" == "E" -o "$sig_selection" == "e" ]; then 	   	

				cd $P0SPICE_ELEC
				mv newoutput.dat $OUT0 	>/dev/null 2>&1
				mv $OUT0 $stoPATH 		>/dev/null 2>&1
				cd $stoPATH
				./CombineFinalOutputs $OUT0
				wait
				cd $P1SPICE_ELEC
				mv newoutput.dat $OUT1 	>/dev/null 2>&1
				mv $OUT1 $stoPATH 		>/dev/null 2>&1
				cd $stoPATH
				./CombineFinalOutputs $OUT1
				wait
				cd $P2SPICE_ELEC
				mv newoutput.dat $OUT2 	>/dev/null 2>&1
				mv $OUT2 $stoPATH 		>/dev/null 2>&1
				cd $stoPATH
				./CombineFinalOutputs $OUT2
				wait
				cd $P3SPICE_ELEC
				mv newoutput.dat $OUT3 	>/dev/null 2>&1
				mv $OUT3 $stoPATH 		>/dev/null 2>&1
				cd $stoPATH
				./CombineFinalOutputs $OUT3
				wait
				cd $P4SPICE_ELEC
				mv newoutput.dat $OUT4 	>/dev/null 2>&1
				mv $OUT4 $stoPATH 		>/dev/null 2>&1
				cd $stoPATH
				./CombineFinalOutputs $OUT4
				wait
				cd $P5SPICE_ELEC
				mv newoutput.dat $OUT5 	>/dev/null 2>&1
				mv $OUT5 $stoPATH 		>/dev/null 2>&1
				cd $stoPATH
				./CombineFinalOutputs $OUT5
				wait
				cd $P6SPICE_ELEC
				mv newoutput.dat $OUT6 	>/dev/null 2>&1
				mv $OUT6 $stoPATH 		>/dev/null 2>&1
				cd $stoPATH
				./CombineFinalOutputs $OUT6
				wait
				mv newoutput.dat $rPATH 

			elif [ "$sig_selection" == "G" -o "$sig_selection" == "g" ]; then

				cd $P0SPICE_GAMM
				mv newoutput.dat $OUT0 >/dev/null 2>&1
				mv $OUT0 $stoPATH >/dev/null 2>&1
				cd $stoPATH
				./CombineFinalOutputs $OUT0
				cd $P1SPICE_GAMM
				mv newoutput.dat $OUT1 >/dev/null 2>&1
				mv $OUT1 $stoPATH >/dev/null 2>&1
				cd $stoPATH
				./CombineFinalOutputs $OUT1
				wait
				cd $P2SPICE_GAMM
				mv newoutput.dat $OUT2 	>/dev/null 2>&1
				mv $OUT2 $stoPATH 		>/dev/null 2>&1
				cd $stoPATH
				./CombineFinalOutputs $OUT2
				wait
				cd $P3SPICE_GAMM
				mv newoutput.dat $OUT3 	>/dev/null 2>&1
				mv $OUT3 $stoPATH 		>/dev/null 2>&1
				cd $stoPATH
				./CombineFinalOutputs $OUT3
				wait
				cd $P4SPICE_GAMM
				mv newoutput.dat $OUT4 	>/dev/null 2>&1
				mv $OUT4 $stoPATH 		>/dev/null 2>&1
				cd $stoPATH
				./CombineFinalOutputs $OUT4
				wait
				cd $P5SPICE_GAMM
				mv newoutput.dat $OUT5 	>/dev/null 2>&1
				mv $OUT5 $stoPATH 		>/dev/null 2>&1
				cd $stoPATH
				./CombineFinalOutputs $OUT5
				wait
				cd $P6SPICE_GAMM
				mv newoutput.dat $OUT6 	>/dev/null 2>&1
				mv $OUT6 $stoPATH 		>/dev/null 2>&1
				cd $stoPATH
				./CombineFinalOutputs $OUT6
				wait
				mv newoutput.dat $rPATH 

			elif [ "$sig_selection" == "P" -o "$sig_selection" == "p" ]; then

				cd $P0SPICE_PROT
				mv newoutput.dat $OUT0 >/dev/null 2>&1
				mv $OUT0 $stoPATH >/dev/null 2>&1
				cd $stoPATH
				./CombineFinalOutputs $OUT0
				wait
				cd $P1SPICE_PROT
				mv newoutput.dat $OUT1 >/dev/null 2>&1
				mv $OUT1 $stoPATH >/dev/null 2>&1
				cd $stoPATH
				./CombineFinalOutputs $OUT1
				wait
				cd $P2SPICE_PROT
				mv newoutput.dat $OUT2 	>/dev/null 2>&1
				mv $OUT2 $stoPATH 		>/dev/null 2>&1
				cd $stoPATH
				./CombineFinalOutputs $OUT2
				wait
				cd $P3SPICE_PROT
				mv newoutput.dat $OUT3 	>/dev/null 2>&1
				mv $OUT3 $stoPATH 		>/dev/null 2>&1
				cd $stoPATH
				./CombineFinalOutputs $OUT3
				wait
				cd $P4SPICE_PROT
				mv newoutput.dat $OUT4 	>/dev/null 2>&1
				mv $OUT4 $stoPATH 		>/dev/null 2>&1
				cd $stoPATH
				./CombineFinalOutputs $OUT4
				wait
				cd $P5SPICE_PROT
				mv newoutput.dat $OUT5 	>/dev/null 2>&1
				mv $OUT5 $stoPATH 		>/dev/null 2>&1
				cd $stoPATH
				./CombineFinalOutputs $OUT5
				wait
				cd $P6SPICE_PROT
				mv newoutput.dat $OUT6 	>/dev/null 2>&1
				mv $OUT6 $stoPATH 		>/dev/null 2>&1
				cd $stoPATH
				./CombineFinalOutputs $OUT6
				wait		 	  		   		   
				mv newoutput.dat $rPATH 

			else
				echo "You shouldn't have seen this. This is super secret text...aborting"
				exit
			fi	
		   	;;
		8)
			if [ "$sig_selection" == "E" -o "$sig_selection" == "e" ]; then 	   	
				cd $P0SPICE_ELEC
				mv newoutput.dat $OUT0 	>/dev/null 2>&1
				mv $OUT0 $stoPATH 		>/dev/null 2>&1
				cd $stoPATH
				./CombineFinalOutputs $OUT0
				wait
				cd $P1SPICE_ELEC
				mv newoutput.dat $OUT1 	>/dev/null 2>&1
				mv $OUT1 $stoPATH 		>/dev/null 2>&1
				cd $stoPATH
				./CombineFinalOutputs $OUT1
				wait
				cd $P2SPICE_ELEC
				mv newoutput.dat $OUT2 	>/dev/null 2>&1
				mv $OUT2 $stoPATH 		>/dev/null 2>&1
				cd $stoPATH
				./CombineFinalOutputs $OUT2
				wait
				cd $P3SPICE_ELEC
				mv newoutput.dat $OUT3 	>/dev/null 2>&1
				mv $OUT3 $stoPATH 		>/dev/null 2>&1
				cd $stoPATH
				./CombineFinalOutputs $OUT3
				wait
				cd $P4SPICE_ELEC
				mv newoutput.dat $OUT4 	>/dev/null 2>&1
				mv $OUT4 $stoPATH 		>/dev/null 2>&1
				cd $stoPATH
				./CombineFinalOutputs $OUT4
				wait
				cd $P5SPICE_ELEC
				mv newoutput.dat $OUT5 	>/dev/null 2>&1
				mv $OUT5 $stoPATH 		>/dev/null 2>&1
				cd $stoPATH
				./CombineFinalOutputs $OUT5
				wait
				cd $P6SPICE_ELEC
				mv newoutput.dat $OUT6 	>/dev/null 2>&1
				mv $OUT6 $stoPATH 		>/dev/null 2>&1
				cd $stoPATH
				./CombineFinalOutputs $OUT6
				wait
				cd $P7SPICE_ELEC
				mv newoutput.dat $OUT7 	>/dev/null 2>&1
				mv $OUT7 $stoPATH 		>/dev/null 2>&1
				cd $stoPATH
				./CombineFinalOutputs $OUT7			   
				wait
				mv newoutput.dat $rPATH 

			elif [ "$sig_selection" == "G" -o "$sig_selection" == "g" ]; then

				cd $P0SPICE_GAMM
				mv newoutput.dat $OUT0 >/dev/null 2>&1
				mv $OUT0 $stoPATH >/dev/null 2>&1
				cd $stoPATH
				./CombineFinalOutputs $OUT0
				wait
				cd $P1SPICE_GAMM
				mv newoutput.dat $OUT1 >/dev/null 2>&1
				mv $OUT1 $stoPATH >/dev/null 2>&1
				cd $stoPATH
				./CombineFinalOutputs $OUT1
				wait
				cd $P2SPICE_GAMM
				mv newoutput.dat $OUT2 	>/dev/null 2>&1
				mv $OUT2 $stoPATH 		>/dev/null 2>&1
				cd $stoPATH
				./CombineFinalOutputs $OUT2
				wait
				cd $P3SPICE_GAMM
				mv newoutput.dat $OUT3 	>/dev/null 2>&1
				mv $OUT3 $stoPATH 		>/dev/null 2>&1
				cd $stoPATH
				./CombineFinalOutputs $OUT3
				wait
				cd $P4SPICE_GAMM
				mv newoutput.dat $OUT4 	>/dev/null 2>&1
				mv $OUT4 $stoPATH 		>/dev/null 2>&1
				cd $stoPATH
				./CombineFinalOutputs $OUT4
				wait
				cd $P5SPICE_GAMM
				mv newoutput.dat $OUT5 	>/dev/null 2>&1
				mv $OUT5 $stoPATH 		>/dev/null 2>&1
				cd $stoPATH
				./CombineFinalOutputs $OUT5
				wait
				cd $P6SPICE_GAMM
				mv newoutput.dat $OUT6 	>/dev/null 2>&1
				mv $OUT6 $stoPATH 		>/dev/null 2>&1
				cd $stoPATH
				./CombineFinalOutputs $OUT6
				wait
				cd $P7SPICE_GAMM
				mv newoutput.dat $OUT7 	>/dev/null 2>&1
				mv $OUT7 $stoPATH 		>/dev/null 2>&1
				cd $stoPATH
				./CombineFinalOutputs $OUT7			   
				wait
				mv newoutput.dat $rPATH 

			elif [ "$sig_selection" == "P" -o "$sig_selection" == "p" ]; then

				cd $P0SPICE_PROT
				mv newoutput.dat $OUT0 >/dev/null 2>&1
				mv $OUT0 $stoPATH >/dev/null 2>&1
				cd $stoPATH
				./CombineFinalOutputs $OUT0
				wait
				cd $P1SPICE_PROT
				mv newoutput.dat $OUT1 >/dev/null 2>&1
				mv $OUT1 $stoPATH >/dev/null 2>&1
				cd $stoPATH
				./CombineFinalOutputs $OUT1
				wait
				cd $P2SPICE_PROT
				mv newoutput.dat $OUT2 	>/dev/null 2>&1
				mv $OUT2 $stoPATH 		>/dev/null 2>&1
				cd $stoPATH
				./CombineFinalOutputs $OUT2
				wait
				cd $P3SPICE_PROT
				mv newoutput.dat $OUT3 	>/dev/null 2>&1
				mv $OUT3 $stoPATH 		>/dev/null 2>&1
				cd $stoPATH
				./CombineFinalOutputs $OUT3
				wait
				cd $P4SPICE_PROT
				mv newoutput.dat $OUT4 	>/dev/null 2>&1
				mv $OUT4 $stoPATH 		>/dev/null 2>&1
				cd $stoPATH
				./CombineFinalOutputs $OUT4
				wait
				cd $P5SPICE_PROT
				mv newoutput.dat $OUT5 	>/dev/null 2>&1
				mv $OUT5 $stoPATH 		>/dev/null 2>&1
				cd $stoPATH
				./CombineFinalOutputs $OUT5
				wait
				cd $P6SPICE_PROT
				mv newoutput.dat $OUT6 	>/dev/null 2>&1
				mv $OUT6 $stoPATH 		>/dev/null 2>&1
				cd $stoPATH
				./CombineFinalOutputs $OUT6
				wait
				cd $P7SPICE_PROT
				mv newoutput.dat $OUT7 	>/dev/null 2>&1
				mv $OUT7 $stoPATH 		>/dev/null 2>&1
				cd $stoPATH
				./CombineFinalOutputs $OUT7			   
				wait 		   		   		   
				mv newoutput.dat $rPATH 

			else
				echo "You shouldn't have seen this. This is super secret text...aborting"
				exit
			fi				   
		   ;;
		*)
		esac
}


#################################################
################ Start Script ###################
#################################################
clear 

echo "      ___________________________________________________"
echo "     /                                                   \\"
echo "     |---------------------------------------------------|"
echo "     |-----------MU2E DATA PROCESSING TOOLKIT------------|"
echo "     |---------------------------------------------------|"
echo "     |------Utilitizing Garfield, ngspice, and ROOT------|"
echo "     |---------------------------------------------------|"
echo "     \___________________________________________________/"
echo ""; echo ""

echo "What do you want to analyze?"
echo "----E)lectons----G)ammas----P)rotons----"
read sig_selection
echo ""; echo ""

echo "Do you want to simulate signals at a fixed energy level or dynamic energy level?"
echo "----F)ixed----D)ynamic----"
read sim_selection
echo ""; echo ""


#echo "Do you want to run ROOT after data is generated?"
#echo "If you choose 'no', you're data will be located in "$rPATH
#echo "----Y)es----N)o----"
#read roo_selection


#################################################
################ FIXED DATA GEN #################
#################################################
if [ "$sim_selection" = "F" -o "$sim_selection" = "f" ]; then

	echo "Do you want to simulate a large data set (20,000 signals) or a small date set (200 signals)?"
	echo "----L)arge----S)mall----"
	read dat_selection
	echo "Removing old .dat files that are not needed for this simulation."
	echo "If you want to save these files, please CTRL-C now, to stop the script"
	echo "5"
	sleep 1
	echo "4"
	sleep 1
	echo "3"
	sleep 1 
	echo "2"
	sleep 1
	echo "1"
	sleep 1
	clear
	echo ""; echo ""
	echo "FULL SPEED A HEAD, CAPT'N!!"
	echo "       _~				"
	echo "    _~ )_)_~			"
	echo "    )_))_))_)			"
	echo "~~~~_!__!__!_~~~~~~~~~"
	echo " ~~~\______t/~~~~~~~~ "
	echo "~~~~~~~~~~~~~~~~~~~~~~"
	echo " ~~~~~~~~~~~~~~~~~~~~ "
	echo "~~~~~~~~~~~~~~~~~~~~~~"
	echo ""; echo ""
	sleep 1

	##############################################
	##############################################
	##############   ELECTRONS   #################
	##############################################
	##############################################

	if [ "$sig_selection" = "E" -o "$sig_selection" = "e" ];
		then		
		#run garfield and generate files
			if [ "$dat_selection" = "L" -o "$dat_selection" = "l" ]; then
		 		echo "Please wait...if you are running 20,000 signals, this make take a long time."
		 		cd $gPATH_FIXED
		 		garfield-9 < "signalrndSr90_electron_large.in" >/dev/null 2>&1

		 		#removes the FIRST line from the generated file. Why? It's just, we don't need it
		 		tail -n +2 $GARF_FILE_E_L > $GARF_FILE_E_L_2
		 		rm $GARF_FILE_E_L
		 		
		 		if [ -f $gPATH_FIXED$GARF_FILE_E_L_2 ]; then
					echo $GARF_FILE_E_L_2" is found! Moving file to be formatted"
					mv $gPATH_FIXED$GARF_FILE_E_L_2 $gFORMATTING
					echo "Formatting files"

					cd $gFORMATTING
					./FormatFiles signalSr90_electron_large2.dat
					mv $GARF_FILE_E_L_2 $gPATH_FIXED
					echo "Moving files to be formatted for spice"

					find $gFORMATTING -name '*.dat' -exec mv {} $sFORMATTING \;

					cd $sFORMATTING
					find ./ -type f -name \*.dat | while read file 
					do
						$sFORMATTING./Garfield2Spice $file
						mv $SIGDAT $sPATH_ELEC
						cd $sPATH_ELEC

						ngspice Preamp.net 	>/dev/null 2>&1  
						PID=$!				>/dev/null 2>&1
						sleep .15			>/dev/null 2>&1 #sleep is required to allow spice time to process
						kill -HUP $PID 		>/dev/null 2>&1

						./CombineOutputs output.dat >/dev/null 2>&1
						cd $sFORMATTING >/dev/null 2>&1
					done 

					cd $sPATH_ELEC
					mv newoutput.dat $rPATH

					if [ "$roo_selection" == "Y" -o "$roo_selection" == "y" ]; then	
						echo "Starting ROOT"
						cd $rPATH
						root -l .L plotsignals.C
					elif [ "$roo_selection" == "N" -o "$roo_selection" == "n" ]; then
						echo "Data generation completed."
					else
						echo "Inputted wrong letter."
						echo "Data generation completed."
					fi
				else
					echo "No .dat generated"
				fi
			fi

		 	if [ "$dat_selection" = "S" -o "$dat_selection" = "s" ]; then
		 		echo $gPATH_FIXED$GARF_FILE_E_S_2" not found! Generating File. Please wait..."
		 		cd $gPATH_FIXED
		 		garfield-9 < "signalrndSr90_electron_small.in" >/dev/null 2>&1

		 		#removes the FIRST line from the generated file
		 		tail -n +2 $GARF_FILE_E_S > $GARF_FILE_E_S_2
		 		rm $GARF_FILE_E_S
		 		
		 		if [ -f $gPATH_FIXED$GARF_FILE_E_S_2 ]; then
					echo $GARF_FILE_E_S_2" is found! Moving file to be formatted"
					mv $gPATH_FIXED$GARF_FILE_E_S_2 $gFORMATTING
					echo "Formatting files"

					cd $gFORMATTING
					./FormatFiles signalSr90_electron_small2.dat
					mv $GARF_FILE_E_S_2 $gPATH_FIXED
					echo "Moving files to be formatted for spice"

					find $gFORMATTING -name '*.dat' -exec mv {} $sFORMATTING \;

					cd $sFORMATTING
					find ./ -type f -name \*.dat | while read file 
					do
						cd $sFORMATTING				
						$sFORMATTING./Garfield2Spice $file
						mv $SIGDAT $sPATH_ELEC
						cd $sPATH_ELEC		 

						ngspice Preamp.net 			>/dev/null 2>&1   
						PID=$! 						>/dev/null 2>&1
						sleep .15					>/dev/null 2>&1 
						kill -HUP $PID 				>/dev/null 2>&1

						./CombineOutputs output.dat 
						wait						
					done 

					cd $sPATH_ELEC
					mv newoutput.dat $rPATH
					if [ "$roo_selection" == "Y" -o "$roo_selection" == "y" ]; then	
						echo "Starting ROOT"
						cd $rPATH
						root -l .L plotsignals.C
					elif [ "$roo_selection" == "N" -o "$roo_selection" == "n" ]; then
						echo "Data generation completed."
					else
						echo "Inputted wrong letter."
						echo "Data generation completed."
					fi
				else
					echo "No .dat generated"
				fi
			fi 

		##############################################
		##############################################
		###############    GAMMAS     ################
		##############################################
		##############################################

	elif [ "$sig_selection" = "G" -o "$sig_selection" = "g" ];
		then
		#run garfield and generate files
			if [ "$dat_selection" = "L" -o "$dat_selection" = "l" ]; then
		 		echo "Please wait...if you are running 20,000 signals, this make take a long time."
		 		cd $gPATH_FIXED
		 		garfield-9 < "signalrndSr90_gamma_large.in" >/dev/null 2>&1
		 		tail -n +2 $GARF_FILE_G_L > $GARF_FILE_G_L_2
		 		rm $GARF_FILE_G_L
		 	
		 		if [ -f $gPATH_FIXED$GARF_FILE_G_L_2 ]; then
					echo $GARF_FILE_G_L_2" is found! Moving file to be formatted"
					mv $gPATH_FIXED$GARF_FILE_G_L_2 $gFORMATTING
					echo "Formatting files"

					cd $gFORMATTING
					./FormatFiles signalSr90_gamma_large2.dat
					mv $GARF_FILE_G_L_2 $gPATH_FIXED
					echo "Moving files to be formatted for spice"

					find $gFORMATTING -name '*.dat' -exec mv {} $sFORMATTING \;

					cd $sFORMATTING
					find ./ -type f -name \*.dat | while read file 
					do
						$sFORMATTING./Garfield2Spice $file
						mv $SIGDAT $sPATH_GAMM
						cd $sPATH_GAMM

						ngspice Preamp.net 	>/dev/null 2>&1   
						PID=$!				>/dev/null 2>&1
						sleep .15			>/dev/null 2>&1
						kill -HUP $PID 		>/dev/null 2>&1

						./CombineOutputs output.dat >/dev/null 2>&1
						cd $sFORMATTING >/dev/null 2>&1
					done 

					cd $sPATH_GAMM
					mv newoutput.dat $rPATH
					if [ "$roo_selection" == "Y" -o "$roo_selection" == "y" ]; then	
						echo "Starting ROOT"
						cd $rPATH
						root -l .L plotsignals.C
					elif [ "$roo_selection" == "N" -o "$roo_selection" == "n" ]; then
						echo "Data generation completed."
					else
						echo "Inputted wrong letter."
						echo "Data generation completed."
					fi
				else
					echo "No .dat generated"
				fi
			fi

		 	if [ "$dat_selection" = "S" -o "$dat_selection" = "s" ]; then
		 		echo $gPATH_FIXED$GARF_FILE_G_S_2" not found! Generating File. Please wait..."
		 		cd $gPATH_FIXED
		 		garfield-9 < "signalrndSr90_gamma_small.in" >/dev/null 2>&1
		 		tail -n +2 $GARF_FILE_G_S > $GARF_FILE_G_S_2
		 		rm $GARF_FILE_G_S
		 		
		 		if [ -f $gPATH_FIXED$GARF_FILE_G_S_2 ]; then
					echo $GARF_FILE_G_S_2" is found! Moving file to be formatted"
					mv $gPATH_FIXED$GARF_FILE_G_S_2 $gFORMATTING
					echo "Formatting files"

					cd $gFORMATTING
					./FormatFiles signalSr90_gamma_small2.dat
					mv $GARF_FILE_G_S_2 $gPATH_FIXED
					echo "Moving files to be formatted for spice"

					find $gFORMATTING -name '*.dat' -exec mv {} $sFORMATTING \;

					cd $sFORMATTING
					find ./ -type f -name \*.dat | while read file 
					do
						$sFORMATTING./Garfield2Spice $file
						mv $SIGDAT $sPATH_GAMM
						cd $sPATH_GAMM
						 
						ngspice Preamp.net 	>/dev/null 2>&1 
						PID=$!				>/dev/null 2>&1
						sleep .15			>/dev/null 2>&1
						kill -HUP $PID 		>/dev/null 2>&1

						./CombineOutputs output.dat >/dev/null 2>&1
						cd $sFORMATTING >/dev/null 2>&1
					done 

					cd $sPATH_GAMM
					mv newoutput.dat $rPATH
					if [ "$roo_selection" == "Y" -o "$roo_selection" == "y" ]; then	
						echo "Starting ROOT"
						cd $rPATH
						root -l .L plotsignals.C
					elif [ "$roo_selection" == "N" -o "$roo_selection" == "n" ]; then
						echo "Data generation completed."
					else
						echo "Inputted wrong letter."
						echo "Data generation completed."
					fi
				else
					echo "No .dat generated"
				fi
			fi 

	##############################################
	##############################################
	###############    PROTONS   #################
	##############################################
	##############################################

	elif [ "$sig_selection" = "P" -o "$sig_selection" = "p" ];
		then			
			#run garfield and generate files
			if [ "$dat_selection" = "L" -o "$dat_selection" = "l" ]; then
		 		echo "Please wait...if you are running 20,000 signals, this make take a long time."
		 		cd $gPATH_FIXED
		 		garfield-9 < "signalrndSr90_proton_large.in" >/dev/null 2>&1
		 		tail -n +2 $GARF_FILE_P_L > $GARF_FILE_P_L_2
		 		rm $GARF_FILE_G_L
		 		
		 		if [ -f $gPATH_FIXED$GARF_FILE_P_L_2 ]; then
					echo $GARF_FILE_P_L_2" is found! Moving file to be formatted"
					mv $gPATH_FIXED$GARF_FILE_P_L_2 $gFORMATTING
					echo "Formatting files"

					cd $gFORMATTING
					./FormatFiles signalSr90_proton_large2.dat
					mv $GARF_FILE_P_L_2 $gPATH_FIXED
					echo "Moving files to be formatted for spice"

					find $gFORMATTING -name '*.dat' -exec mv {} $sFORMATTING \;

					cd $sFORMATTING
					find ./ -type f -name \*.dat | while read file 
					do
						$sFORMATTING./Garfield2Spice $file
						mv $SIGDAT $sPATH_PROT
						cd $sPATH_PROT

						ngspice Preamp.net 	>/dev/null 2>&1 
						PID=$!				>/dev/null 2>&1
						sleep .15			>/dev/null 2>&1
						kill -HUP $PID 		>/dev/null 2>&1

						./CombineOutputs output.dat >/dev/null 2>&1
						cd $sFORMATTING >/dev/null 2>&1
					done 

					cd $sPATH_PROT
					mv newoutput.dat $rPATH
					if [ "$roo_selection" == "Y" -o "$roo_selection" == "y" ]; then	
						echo "Starting ROOT"
						cd $rPATH
						root -l .L plotsignals.C
					elif [ "$roo_selection" == "N" -o "$roo_selection" == "n" ]; then
						echo "Data generation completed."
					else
						echo "Inputted wrong letter."
						echo "Data generation completed."
					fi

				else
					echo "No .dat generated"
				fi
			fi

		 	if [ "$dat_selection" = "S" -o "$dat_selection" = "s" ]; then
		 		echo $gPATH_FIXED$GARF_FILE_P_S_2" not found! Generating File. Please wait..."
		 		cd $gPATH_FIXED
		 		garfield-9 < "signalrndSr90_proton_small.in" >/dev/null 2>&1
		 		tail -n +2 $GARF_FILE_P_S > $GARF_FILE_P_S_2
		 		rm $GARF_FILE_P_S
		 	
		 		if [ -f $gPATH_FIXED$GARF_FILE_P_S_2 ]; then
					echo $GARF_FILE_P_S_2" is found! Moving file to be formatted"
					mv $gPATH_FIXED$GARF_FILE_P_S_2 $gFORMATTING
					echo "Formatting files"

					cd $gFORMATTING
					./FormatFiles signalSr90_proton_small2.dat
					
					mv $GARF_FILE_P_S_2 $gPATH_FIXED
					echo "Moving files to be formatted for spice"

					find $gFORMATTING -name '*.dat' -exec mv {} $sFORMATTING \;

					cd $sFORMATTING
					find ./ -type f -name \*.dat | while read file 
					do
						$sFORMATTING./Garfield2Spice $file
						mv $SIGDAT $sPATH_PROT
						cd $sPATH_PROT					 

						ngspice Preamp.net 	>/dev/null 2>&1    
						PID=$!				>/dev/null 2>&1
						sleep .15			>/dev/null 2>&1
						kill -HUP $PID 		>/dev/null 2>&1

						./CombineOutputs output.dat >/dev/null 2>&1
						cd $sFORMATTING >/dev/null 2>&1
					done 

					cd $sPATH_PROT
					mv newoutput.dat $rPATH
					
					if [ "$roo_selection" == "Y" -o "$roo_selection" == "y" ]; then	
						echo "Starting ROOT"
						cd $rPATH
						root -l .L plotsignals.C
					elif [ "$roo_selection" == "N" -o "$roo_selection" == "n" ]; then
						echo "Data generation completed."
					else
						echo "Inputted wrong letter."
						echo "Data generation completed."
					fi

				else
					echo "No .dat generated"
				fi
			fi 
	else
		echo "An error occured or your sig_selection was incorrect" 
	fi

	echo ""; echo ""

#################################################
############## DYNAMIC DATA GEN #################
#################################################

elif [ "$sim_selection" = "D" -o "$sim_selection" = "d" ]; then

	echo "How many cores does your system have?"
	echo "----1)--2)--3)--4)--5)--6)--7)--8)----"
	read core_selection
	echo ""; echo ""

	echo "How many iterations do you want to run in garfield?"
	echo "Please enter an integer"
	read iterations
	echo ""; echo ""
	echo "Removing old .dat files that are not needed for this simulation."
	echo "If you want to save these files, please CTRL-C now, to stop the script"
	#echo "5"
	#sleep 1
	#echo "4"
	#sleep 1
	echo "3"
	sleep 1 
	echo "2"
	sleep 1
	echo "1"
	sleep 1
	clear
	echo ""; echo ""
	echo "FULL SPEED A HEAD, CAPT'N!!"
	echo "       _~				"
	echo "    _~ )_)_~			"
	echo "    )_))_))_)			"
	echo "~~~~_!__!__!_~~~~~~~~~"
	echo " ~~~\______t/~~~~~~~~ "
	echo "~~~~~~~~~~~~~~~~~~~~~~"
	echo " ~~~~~~~~~~~~~~~~~~~~ "
	echo "~~~~~~~~~~~~~~~~~~~~~~"
	echo ""; echo ""

	chooseProcessAmount 
	wait

	#COMBINE is the exit code from the chooseProcessAmount function.
	#It is still a work in progress at the moment
	echo "Combining files"
	sleep 1
	combineFiles $COMBINE
	wait

	#if [ "$roo_selection" == "Y" -o "$roo_selection" == "y" ]; then	
	#	echo "Starting ROOT"
	cd $rPATH
	./plotsignals_histo
	#elif [ "$roo_selection" == "N" -o "$roo_selection" == "n" ]; then
	#	echo "Data generation completed."
	#else
	#	echo "Inputted wrong letter."
	#	echo "Data generation completed."
	#fi
	
else
	echo "Incorrect selection; try again"
	exit
fi

exit
bash

# ***********************************************************
# *****                   			                        *
# ****                     .sssssssss.			           **
# ***                 .sssssssssssssssssss		          ***
# **               sssssssssssssssssssssssss	         ****
# *               ssssssssssssssssssssssssssss		    *****
# *                @@sssssssssssssssssssssss@ss 	    *****
# *                |s@@@@sssssssssssssss@@@@s|s 	    *****
# *         _______|sssss@@@@@sssss@@@@@sssss|s		    *****
# *       /         sssssssss@sssss@sssssssss|s 	    *****
# *      /  .------+.ssssssss@sssss@ssssssss.| 		    *****
# *     /  /       |...sssssss@sss@sssssss...|  		*****
# *    |  |        |.......sss@sss@ssss......|  	    *****
# *    |  |        |..........s@ss@sss.......|  	    *****
# *    |  |        |...........@ss@..........|  	    *****
# *     \  \       |............ss@..........|  	    *****
# *      \  '------+...........ss@...........|  	    *****
# *       \________ .........................|  	    *****
# *                |..........................|       	*****
# *               /..........6/27/2012.........\ 	    *****
# **             |.............SIST............|	     ****
# ***               |........Fermilabs.......|	  	      ***
# ****                  |......Mu2e.....|		           **
# *****	            Beer...it's man juice (TM)	            *
# ***********************************************************



