#ifndef __PLOTSIGNALS_FUNCTIONS
#define __PLOTSIGNALS_FUNCTIONS

/* plotsignals_functions.h

  -Header provides various functions that can be used to manipulate your input data
  Modified By: Daniel Kulas

*/

#include <fstream>
#include <vector>
#include <iostream>
#include <math.h>
#include <vector>
#include "TH1D.h"
#include "TGraph.h"
#include "TCanvas.h"
#include "TRandom3.h"
#include "TObject.h"
#include "TLine.h"
#include "TMath.h"

#define PI	3.1415926535
#define TEMPERATURE 300
#define RESISTANCE  300

using namespace std;


//creates canvas and histograms
TCanvas *c1 = new TCanvas("c1","Plot Histograms",1200,800);
TCanvas *c2 = new TCanvas("c2","Plot graphs", 1200,800);

TH1D  	*h1 = new TH1D("h1","Maximum value for a signal ",400,2.3,2.5);
TH1D  	*h2 = new TH1D("h2","Time distribution",100,-0.01,0.01);

TGraph* grhp1;
TGraph* grhp2;
TGraph* grhp3;
TGraph* grhp4;

TLine *baseLine  = new TLine(0,0,0.25,0);		      //shows were 0 is relative to signal
TLine *thresLine;
TLine *thresLine2;
TLine *adcHitLine;



TRandom3 *ran   = new TRandom3();
TRandom  *noise = new TRandom();

Double_t newThreshold_sig1, newThreshold_sig2, newThreshold_sig3, fixedThres;

Double_t maxsig = 0;
Double_t adcHit;

float noiseCurr = 0;

// threshold: -0.11 @ 30MHz  -0.21 @ 100MHz, -0.26 @ 200MHz, <----Used only if dynamic threshold is taken out

bool highFilter, lowFilter;




/*==================Filters========================
  =================================================*/
//Applys a highpass filter to the input signal
void highpass(Double_t* x, int n, Double_t dt, Double_t frequency, Double_t R, Double_t* y)
{
    Double_t RC = 1/(2 * PI * frequency);
    Double_t alpha = (RC / (RC + dt));
    y[0] = 0;
    for (int i = 1 ; i < n; i++)
    {
        y[i] = alpha * y[i-1] + alpha * (x[i] - x[i-1]) + R * x[i];
    }
};

//Applys a lowpass filter to the input signal
void lowpass(Double_t* x, int n, Double_t dt, Double_t frequency, Double_t* y)
{
    Double_t RC = 1/(2 * PI * frequency);
    Double_t alpha = dt / (RC + dt);
    y[0] = 0;
    for(int i = 1; i < n; i++)
    {
        y[i] = alpha * x[i] + (1 - alpha) * y[i-1];
    }
};

Double_t adcdigi(int n, Double_t* x, Double_t* y)
{
    Double_t maxsignal = 0;
    Double_t freq = 30;   //MHz
    Double_t ranVar = ran->Rndm();
    Double_t dt = x[n-2] - x[n-3];
    Int_t adcfreq = 1/(freq * dt);                //adc frequency
    Int_t startdigi = adcfreq * ranVar;         //conversion starts at a random point

    for (int i = 0 ; i < n ; i++)
    {
        Int_t startConversion = i-startdigi;    //starting point of conversion at random time
        if ( startConversion % adcfreq == 0 )
        {
            if (y[i] < maxsignal)
            {
                maxsignal = y[i];
                adcHit = x[i];
            }
        }
    }
    //cout << "maxsignal from ADC: " << maxsignal << endl;
    /* if(maxsignal < getFixedThres())
     {
         if(maxsignal < getFixedThres() || maxsignal < getNewThreshold_sig1() || maxsignal < getNewThreshold_sig2())
             return maxsignal;
         return maxsignal;
     }

     else
         return 1;*/
    return maxsignal;
};

Double_t findMax(int n, vector<double> y)
{
    Double_t maxsignal = 0;
    for(int i = 0; i < n; i++)
    {
        if(y.at(i) > maxsignal)            //CHANGED SIGN
        {
            maxsignal = y.at(i);
        }
    }
    cout << maxsignal << endl;
    return maxsignal;
};

Double_t getadcHit()
{
    return adcHit;
};


/*==================Threshold======================
  ===================delta t=======================*/

//fix logic to account for different noise levels
void setUserFixedThres(int userVal)
{
    if(userVal == 30)
        fixedThres = -0.11;
    else if(userVal == 100)
        fixedThres = -0.21;
    else if(userVal == 200)
        fixedThres = -0.26;
    else
        fixedThres = -0.21;
}

//dumb code..forgot what it does but I'm sure  has some purpose
void setFixedThres()
{
    fixedThres = fixedThres;
}

Double_t getFixedThres()
{
    return fixedThres;
}

void setMaxVar()
{
    maxsig = 0;
}

void setNewThres_sig1(int n, Double_t* y)
{
    newThreshold_sig1 = 0;   //reset old values
    maxsig = 0;

    for(int i = 0; i < n; i++)
    {
        if(y[i] < maxsig)
        {
            maxsig = y[i];
        }
    }

    if(maxsig >= -0.21)
    {
        // cout << "Don't change the threshold. Signal does not cross fixed threshold" << endl;
    }
    if(maxsig <= -0.21)
    {
        // cout << "Signal crosses fixed threshold" << endl;
        newThreshold_sig1 = maxsig/2;
        if(newThreshold_sig1 >=-0.21)
        {
            //    cout << "New threshold at: " << newThreshold_sig1 << endl;
            //      cout << "New threshold is lower than fixed threshold. Going back to fixed threshold" << endl;
            newThreshold_sig1 = fixedThres;
        }
        //cout << "Threshold set at: " << newThreshold_sig1 << " uA on signal 1" << endl;
    }
    // cout << "" << endl;
}

void setNewThres_sig2(int n, Double_t* y)
{
    newThreshold_sig2 = 0;   //reset old values
    maxsig = 0;

    for(int i = 0; i < n; i++)
    {
        if(y[i] < maxsig)
        {
            maxsig = y[i];
        }
    }

    if(maxsig >= -0.21)
    {
        //     cout << "Don't change the threshold. Signal does not cross fixed threshold" << endl;
    }
    if(maxsig <= -0.21)
    {
        //cout << "Signal crosses fixed threshold" << endl;
        newThreshold_sig2 = maxsig/2;
        if(newThreshold_sig2 >= -0.21)
        {
            // cout << "New threshold at: " << newThreshold_sig2 << endl;
            // cout << "New threshold is lower than fixed threshold. Going back to fixed threshold" << endl;
            newThreshold_sig2 = fixedThres;
        }
         cout << "Threshold set at: " << newThreshold_sig2 << " uA on signal 2" << endl;
    }
    // cout << "" << endl;
}

void setNewThres_sig3(int n, Double_t* y)
{
    newThreshold_sig3 = 0;   //reset old values
    maxsig = 0;

    for(int i = 0; i < n; i++)
    {
        if(y[i] < maxsig)
        {
            maxsig = y[i];
        }
    }

    if(maxsig >= getFixedThres())
    {
        // cout << "Don't change the threshold. Signal does not cross fixed threshold" << endl;
    }
    if(maxsig <= getFixedThres())
    {
        //  cout << "Signal crosses fixed threshold" << endl;
        newThreshold_sig3 = maxsig/2;
        if(newThreshold_sig3 >= getFixedThres())
        {
            //    cout << "New threshold at: " << newThreshold_sig3 << endl;
            //    cout << "New threshold is lower than fixed threshold. Going back to fixed threshold" << endl;
            newThreshold_sig3 = fixedThres;
        }
          cout << "Threshold set at: " << newThreshold_sig3 << " uA on signal 3 [no noise]" << endl;
    }
     cout << "" << endl;
}

Double_t getNewThreshold_sig1()
{
    return newThreshold_sig1;
}

Double_t getNewThreshold_sig2()
{
    return newThreshold_sig2;
}

Double_t getNewThreshold_sig3()
{
    return newThreshold_sig3;
}

//cycle through signal. Checks each value to see if it goes over threshold
//If so, break out of function
//If not, signal missed threshold
bool did_sig_miss_thres(int n, Double_t* y)
{
    Double_t maxVal = 0;

    for(int i = 0; i < n; i++)
    {
        if(y[i] <= maxVal)
        {
            maxVal = y[i];
        }
    }
    if(maxVal < getFixedThres())
    {
        //   cout << "Signal did not miss threshold" << endl;
        return false;
    }

    return true;
};

//calcTime1 & calcTime2 return the time when the signal crosses the threshold for the first time
//Only calculates time once per signal (assume if crosses threshold)
Double_t calcTime1(int n, Double_t* x, Double_t* y)
{
    long double time_1;
    for (int i = 0 ; i < n ; i++)
    {
        if( y[i] < getFixedThres() )
        {

            if ( y[i] < getNewThreshold_sig1())     //if signal is greater than threshold, calc delta t
            {
                time_1 = x[i];
                //   cout << "Threshold for sig 1: " << getNewThreshold_sig1() << " At time = " << x[i] << " us" << endl;
                return time_1;
            }
        }
    }
    return time_1;
};

Double_t calcTime2(int n, Double_t* x, Double_t* y)
{
    long double time_2;
    for (int i = 0 ; i < n ; i++)
    {
        if( y[i] < getFixedThres() )
        {
            if ( y[i] < getNewThreshold_sig2())
            {
                time_2 = x[i];
                //  cout << "Threshold for sig 2: " << getNewThreshold_sig2() << " At time = " << x[i] << " us" << endl;
                //   cout << "" << endl;
                return time_2;
            }
        }
    }
    return time_2;
};


Double_t calcRMS(Double_t n, Double_t* y)
{
    Double_t sumY = 0, rms;

    for(int i = 0; i < n; i++)
    {
        sumY += pow(y[i], 2);
    }
    return rms = sqrt(sumY/n);
};

void setNoiseCurr(int userBand)
{
    noiseCurr = sqrt((4*(TMath::K())*TEMPERATURE*userBand*1E6)*300)/300;
   // cout<<"R="<<sqrt(4*TMath::K()*TEMPERATURE*300)<<endl;
    cout << "The current noise is set at " << noiseCurr*1E6 << " uA." << endl;
};

Double_t getNoiseCurr()
{
    return noiseCurr*1E6;
};

void DEBUG()
{
    cout << "TESTING" << endl;
};

#endif