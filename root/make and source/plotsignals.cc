/*
	plotsignals.C

	--ROOT program to support ConnectPrograms.sh
		Streamlined downed compared to the main ROOT program
	Created By: Daniel Kulas
				7/19/12
				Mu2e

															*/

//#include "plotsignals_functions.hh"
#include "TApplication.h"
#include <fstream>
#include <vector>
#include <iostream>
#include <math.h>
#include <vector>
#include "TH1D.h"
#include "TGraph.h"
#include "TCanvas.h"
#include "TRandom3.h"
#include "TObject.h"
#include "TLine.h"
#include "TMath.h"
#include "TH1.h"
#include "TFile.h"


using namespace std;

TCanvas *c1;
//TH1D    *h2 = new TH1D("h2","Time distribution",100,-0.01,0.01);

Double_t findMax(int n, vector<double> y)
{
    Double_t maxsignal = 0;
    for(int i = 0; i < n; i++)
    {
        if(y[i] > maxsignal)            //CHANGED SIGN
        {
            maxsignal = y.at(i);
        }
    }
   // cout << maxsignal << endl;
    return maxsignal;
};


//Draw graphs
void plotsignals_graphs(const char* theFile)
{
	TCanvas *c2 = new TCanvas("c2","Plot graphs", 1200,800);
	
	int display = 1,				//to display the graphs
		count 	= 0,				//to keep track of your vector position
		n 		= 0;				//to keep track of what line you are on 

	unsigned int notfound = -1;		//says unsigned but declares a signed value. Sense. It makes none. I know. 
									//It just removes the warning message when using the find() function
	char semn[5];					//this is just used to store junk we don't need
									//if your data generated for some reason has more stuff than I use but you get weird segmentation errors,
									//increase the size of this array
	char *cstr 	= "";
	char *s    	= new char[1];
	bool doplot = false;
	string line, templine;

	float x[1000000];				//would prefer to use vectors but TGraph doesn't accept vectors as (x,y) coordinates
	float y[1000000];				//this could potentially run out of space for the array if your data set is large enough

	float time, amps;

	vector<string> value;

	ifstream fp;
	fp.open(theFile);

	//need to do some tracing in the input file. It's not in a ROOT friendly format
	while(!fp.eof())
	{	
		getline(fp,line);
		if(line.find("Values") != notfound)			//if you do find "Values"
		{
			n = 0;
			while(line.find("Title") == notfound)
			{
				getline(fp,line);

				if(line.find("Title") != notfound)	//always check if you find "Title"; indicates a new signal set. 
					break;							//So break and process the data you collected

				value.push_back(line);				//get line, store in vector, format string to char*, read through cstr to get data
				templine = value[count];
				count++;
				cstr = new char[templine.size()+1];
				strcpy(cstr, templine.c_str());		//yes, I know...C functions in C++...sue me
				sscanf(&cstr[0], "%s %e", semn, &time);

				x[n] = time;

				memset(cstr, 0, sizeof(cstr));		//clear out char* object to prevent memory leak
				templine.clear();

				getline(fp,line);

				if(line.find("Title") != notfound)
					break;

				value.push_back(line);
				templine = value[count];
				count++;
				cstr = new char[templine.size()+1];
				strcpy(cstr, templine.c_str());
				sscanf(&cstr[0], "%e", &amps);

				y[n] = amps;

				n++;		//increment n now to indicate the collection both (x,y) values
							//if unsure of the code, take a look a the "newoutput.dat" file in the root folder, it'll make sense
				memset(cstr, 0, sizeof(cstr));		
				templine.clear();

				getline(fp,line);

				if(line.find("Title") != notfound)
					break;

				value.push_back(line);		//after the amp value, is a blank space...push the vector to get that blank space and start the loop over
				templine = value[count];
				count++;

				if(amps != 0)
					doplot = true;
			}//end while	

			c2->Clear();
			c2->Divide(1,1);	//if you have more graphs to process, just change these numbers 
			c2->cd(1);

			TGraph* gr = new TGraph(n,x,y);
            gr->GetYaxis()->SetTitle("I(#muA)");
            gr->GetXaxis()->SetTitle("t(#mus)");
            gr->SetTitle("Initial Signal");

            if (display == 1)
            {
                gr->Draw();
                //cout << "update "<<endl;
                c2->Update();

                TFile *grps = new TFile("Graphs.root", "RECREATE");
				gr->Write();
				grps->Close();
            }     			
		}//end if

		if (display == 1)
        {
            if (doplot)
            {
            
              //  gets(s);
            }
        }
	}//end while
}//end function

//generate histograms
void plotsignals_histo(const char* theFile)
{
	c1 = new TCanvas("c1","Plot Histograms",1200,800);
	TH1D    *h1 = new TH1D("h1","Maximum value for a signal ",150,0,150);	

	int count = 0,				
		n 	  = 0;				

	unsigned int notfound = -1;		 
									
	char semn[5];					
									
									
	char *cstr 	= "";
	char *s    	= new char[1];
	bool doplot = false;
	string line, templine;
				
	vector<double> x;
	vector<double> y;

	float time, amps;
	float maxsig = 0;

	vector<string> value;

	ifstream fp;
	fp.open(theFile);


	while(!fp.eof())
	{
		getline(fp,line);
		if(line.find("Values") != notfound)			
		{
			n = 0;
			    
			while(line.find("Title") == notfound && !fp.eof())
			{
				getline(fp,line);

				if(line.find("Title") != notfound)	
					break;							

				value.push_back(line);				
				templine = value[count];
				count++;
				cstr = new char[templine.size()+1];
				strcpy(cstr, templine.c_str());		
				sscanf(&cstr[0], "%s %e", semn, &time);

				x.push_back(time);

				delete[] cstr;		
				templine.clear();

				getline(fp,line);

				if(line.find("Title") != notfound)
					break;

				value.push_back(line);
				templine = value[count];
				count++;
				cstr = new char[templine.size()+1];
				strcpy(cstr, templine.c_str());
				sscanf(&cstr[0], "%e", &amps);

				y.push_back(amps * 1000);	//convert to mVun

				n++;

				delete[] cstr;		
				templine.clear();

				getline(fp,line);

				if(line.find("Title") != notfound)
					break;

				value.push_back(line);		
				templine = value[count];
				count++;

				if(amps != 0)
					doplot = true;
				   
			}//end while	
				  
			maxsig = 0;
			c1->Clear();
			c1->Divide(1,1);

			c1->cd(1);
			Double_t maxsignal = findMax(n,y);
			//cout << maxsignal << endl;
			h1->Fill(maxsignal);
			h1->GetYaxis()->SetTitle("Occurrences");
            h1->GetXaxis()->SetTitle("I(#muA)");
            h1->Draw();
  		 

            x.clear();
            y.clear();

            n = 0;
            doplot = false;
           // c1->Update();
        }
    	//c1->Update();
    }

    value.clear(); 
    templine.clear();
    c1->Update();
   
    TFile *maxSigFile = new TFile("MaxSignal.root", "RECREATE");
	h1->Write();
	maxSigFile->Close();

	//TFile *timeFile = new TFile("[FILE NAME HERE].root", "RECREATE");
	//h2->Write();
	//timeFile->Close();
	

}

int main(int argc, char **argv)
{

	//Fake it till you make it!
	int fake_argc = 2;	
  	char *fake_argv[2] = {"falsename", "-b"};
	TApplication *theApp = new TApplication("Plotsignals", &fake_argc, fake_argv);

	TCanvas *c2 = new TCanvas("c2","Plsdsdot Histograms",1200,800);

		
	cout << "Starting root..." << endl;

	const char* theFile = "newoutput.dat";
	cout << "Plotting histograms" << endl;
	plotsignals_histo(theFile);
	//cout << "Plotting graphs" << endl;
	//plotsignals_graphs(theFile);	
	return 0;
}
