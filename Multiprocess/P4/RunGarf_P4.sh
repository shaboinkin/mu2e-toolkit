####################################################
#	Probably an overly complicated approach        #
#   to multiprocessing...BUT IT SHALL BE DONE!!!   #
#												   #
# 	Created By: Daniel Kulas 					   #
#				7/17/12							   #
#			For: ConnectPrograms.sh      		   #
#                 mu2e							   #
####################################################


#THINGS TO NOTE!!!!!!!!!!!!
#	$1 = iterations parameter
#	$2 = signal selection parameter 

##inits##
mPATH=$HOME/mu2e_toolkit_3.0
mutliPATH=$mPATH/Multiprocess
P4=$mutliPATH/P4

rPATH=$mPATH/root
stoPATH=$rPATH/data_storage

gPATH=$P4/garfield/
fPATH=$P4/formatting

gFORMATTING=$fPATH/gFiles/Storage/
sFORMATTING=$fPATH/sFiles/Storage/

sPATH=$P4/spice/Preamp
sPATH_ELEC=$sPATH/electrons/
sPATH_PROT=$sPATH/protons/
sPATH_GAMM=$sPATH/gammas/
#################################################
########change energy files######################
GARF_FILE_E="signalSr90_electron.dat"			
GARF_FILE_G="signalSr90_gamma.dat"
GARF_FILE_P="signalSr90_proton.dat"

GARF_FILE_E_2="signalSr90_electron2.dat"
GARF_FILE_G_2="signalSr90_gamma2.dat"
GARF_FILE_P_2="signalSr90_proton2.dat"

GARF_FILE_E_NEW="signalSr90_electron_new.dat"
GARF_FILE_G_NEW="signalSr90_gamma_new.dat"
GARF_FILE_P_NEW="signalSr90_proton_new.dat"
#################################################
#################################################

SIGDAT="signal.dat"
OUT="output.dat"

TOTAL=105

find $gPATH -name '*.dat' -exec rm {} \;
find $sFORMATTING -name '*.dat' -exec rm {} \;
find $sPATH_ELEC -name '*.dat' -exec rm {} \;
find $sPATH_GAMM -name '*.dat' -exec rm {} \;
find $sPATH_PROT -name '*.dat' -exec rm {} \;

if [ "$2" = "E" -o "$2" = "e" ]; 
	then
		cd $gPATH
		echo "Please wait while data is being generated"
		for(( c=1; c<=$1; c++))
		do 
			energy=$(( $RANDOM % TOTAL +1))
			./ModGarf_elec $energy
			garfield-9 < "signalrndSr90_electron.in" >/dev/null 2>&1
			tail -n +2 $GARF_FILE_E > $GARF_FILE_E_2
			./CombineOutputs_elec $GARF_FILE_E_2
			rm $GARF_FILE_E_2
			rm $GARF_FILE_E
		done 
		#running the CombineOutputs file will change the name back into signalSr90_electron_new.dat
		if [ -f $GARF_FILE_E_NEW ]; 
			then
				mv $gPATH$GARF_FILE_E_NEW $gFORMATTING
				cd $gFORMATTING
				./FormatFiles $GARF_FILE_E_NEW
				#move original file back to where it was generated at, prevents errors
				mv $GARF_FILE_E_NEW $gPATH
				#move all .dat files from $gFORMATTING to $sFORMATTING to be processed
				find $gFORMATTING -name '*.dat' -exec mv {} $sFORMATTING \;

				cd $sFORMATTING
				echo "Spicing it up P4"
				find ./ -type f -name \*.dat | while read file 
				do
					$sFORMATTING./Garfield2Spice $file 
					mv $SIGDAT $sPATH_ELEC
					cd $sPATH_ELEC

					ngspice Preamp.net  >/dev/null 2>&1  
					PID=$! 				>/dev/null 2>&1 		
					sleep .15			>/dev/null 2>&1 		
					kill -HUP $PID 		>/dev/null 2>&1 		

					./CombineOutputs output.dat
					cd $sFORMATTING						
				done 
				echo "Done spicing it P4"
				sleep .12
		else
			echo "No .dat generated"
		fi

elif [ "$2" = "G" -o "$2" = "g" ];
	then
		cd $gPATH
		echo "Please wait while data is being generated"
		for(( c=1; c<=$1; c++))
		do 
			energy=$(( $RANDOM % TOTAL +1))
			./ModGarf_gamm $energy
			garfield-9 < "signalrndSr90_gamma.in" >/dev/null 2>&1
			tail -n +2 $GARF_FILE_G > $GARF_FILE_G_2
			./CombineOutputs_gamm $GARF_FILE_G_2
			rm $GARF_FILE_G_2
			rm $GARF_FILE_G
		done

		if [ -f $GARF_FILE_G_NEW ]; 
			then
				mv $gPATH$GARF_FILE_G_NEW $gFORMATTING
				cd $gFORMATTING

				./FormatFiles $GARF_FILE_G_NEW
				mv $GARF_FILE_G_NEW $gPATH
				find $gFORMATTING -name '*.dat' -exec mv {} $sFORMATTING \;

				cd $sFORMATTING
				echo "Spicing it up P4"
				find ./ -type f -name \*.dat | while read file 
				do
					$sFORMATTING./Garfield2Spice $file 
					mv $SIGDAT $sPATH_GAMM
					cd $sPATH_GAMM

					ngspice Preamp.net  >/dev/null 2>&1  
					PID=$! 				>/dev/null 2>&1 		
					sleep .15			>/dev/null 2>&1 		
					kill -HUP $PID 		>/dev/null 2>&1 		

					./CombineOutputs output.dat
					cd $sFORMATTING						
				done 
				echo "Done spicing it P4"
				sleep .12
		else
			echo "No .dat generated"
		fi

elif [ "$2" = "P" -o "$2" = "p" ];
	then
		cd $gPATH
		echo "Please wait while data is being generated"
		for(( c=1; c<=$1; c++))
		do 
			energy=$(( $RANDOM % TOTAL +1))
			./ModGarf_prot $energy
			garfield-9 < "signalrndSr90_proton.in" >/dev/null 2>&1
			tail -n +2 $GARF_FILE_P > $GARF_FILE_P_2
			./CombineOutputs_prot $GARF_FILE_P_2
			rm $GARF_FILE_P_2
			rm $GARF_FILE_P
		done 

		if [ -f $GARF_FILE_P_NEW ]; 
			then
				mv $gPATH$GARF_FILE_P_NEW $gFORMATTING
				cd $gFORMATTING

				./FormatFiles $GARF_FILE_P_NEW
				mv $GARF_FILE_P_NEW $gPATH
				find $gFORMATTING -name '*.dat' -exec mv {} $sFORMATTING \;

				cd $sFORMATTING
				echo "Spicing it up P4"
				find ./ -type f -name \*.dat | while read file 
				do
					$sFORMATTING./Garfield2Spice $file 
					mv $SIGDAT $sPATH_PROT
					cd $sPATH_PROT

					ngspice Preamp.net  >/dev/null 2>&1  
					PID=$! 				>/dev/null 2>&1 		
					sleep .15			>/dev/null 2>&1 		
					kill -HUP $PID 		>/dev/null 2>&1 		

					./CombineOutputs output.dat
					cd $sFORMATTING						
				done 
				echo "Done spicing it P4"
				sleep .12
		else
			echo "No .dat generated"
		fi
else
	echo "Something is wrong with your signal selection choice...aborting..."
	exit
fi

exit
bash 			
