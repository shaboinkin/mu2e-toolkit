
mPATH=$HOME/mu2e_toolkit_2.0
sPATH=$mPATH/spice/LowPassFilter/buffer1
fPATH=$mPATH/formatting
sFORMATTING=$HOME/mu2e_toolkit_2/formatting/sFiles/Storage/

cd $sFORMATTING

find ./ -type f -name \*.dat | while read file 
do
	
	./Garfield2Spice $file
	mv $file $sPATH
	cd $sPATH

#	ngspice LowPassFilter.net >/dev/null 2>&1 &
#	PID=$!
#	sleep .15
#	kill -HUP $PID
#	./CombineOutputs output.dat

	cd $sFORMATTING
done 

exit
bash