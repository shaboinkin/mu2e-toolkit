*******************************************************
* This script plots the arrival time distribution     *
* for the first electron from a 100MeV electron       *
* track through the drift tube.			      *
*						      *
* NOTE:	Garfield help pages can be found at           *
*	http://consult.cern.ch/writeup/garfield/help/ *
*******************************************************


Global threshold=-0.2

***************** CELL *******************************
******************************************************
&CELL
Global xoff=0.0000
Global yoff=0.0000
Global rWire = 0.00125
Global Bx = 0.


** Create a tub with r=.25cm and V=0V
Tube r 0.25 v 0 
** Create a wire with the rows command
** syntax:
** 	Rows
**	label n diameter x y [V [weight [length [density]]]]
**	(blank line)
Rows
  s 1 2*{rWire} {xoff} {yoff} 1400

****************** MAGNETIC **************************
******************************************************
&Mag
** set the components of the magnetic field.
comp {Bx} 0 0 T

**************** GAS *********************************
******************************************************
&GAS
Global temp=295
Global p = 1
** set the pressure.
pressure {p} bar
Global gas_file=`Ar80-CO220--B{Bx}T--P{p}bar.gas`
Global gas_member `exb`
** if the gas file exists, use it because a call to
**	Magboltz can take a long time. 
Call inquire_member(gas_file,gas_member,`gas`,exist)
If exist then
   get {gas_file,gas_member}
Else
  ** Invoke Magboltz and save the data.
  write {gas_file,gas_member}
  Magboltz argon 80 co2 20 e-field-range 100 500000 ...
	n-e 15 coll 10 mobility 1
Endif

** mobility taken from cern-thesis-98-021
** mobility is in units of cm2/Vs and ep in units of V/cm torr

    
Read-vector ep mobility
<mobility.mob

** transform to V/cm2 musec which is the garfield unit
Global mobility=mobility*1e-6
add ion-mobility mobility vs ep




**  Call HEED for simulation of ionization of a particle
**	transversing through the gas
Heed argon 80 co2 20

******************** OPTIMISE ************************
******************************************************
&Opt
** set the penning transfer rate of all excited Ar
**	to 30%
penning-transf Ar* 30




&DRIFT
Int-par int-acc 1e-10 mc-dist 0.002 projected-path compute-if-interpolation-fails
**Int-par int-acc 1e-10


Global ydrift=0.11
Global xdrift=SQRT(0.25*0.25-ydrift*ydrift)

track -{xdrift} {ydrift} 0 {xdrift} {ydrift} 0 electron energy 100 MeV
track heed
clustering-histograms iterations 1000


&SIGNAL




  For i From 1 To 1 Do		

window 0.0 0.0001 2000

Global ypos=0.25*rnd_uniform
Global xpos = SQRT(0.25*0.25-ypos*ypos)

say 'Position={xpos} {ypos}'


**track -{xpos} {ypos} 0 {xpos} {ypos} 0 proton energy 10 MeV ...
**        delta-electrons notrace-delta-electrons 

track -{xpos} {ypos} 0 {xpos} {ypos} 0 electron energy 100 MeV ...
        delta-electrons notrace-delta-electrons 

**track -{xpos} {ypos} 0 {xpos} {ypos} 0 gamma energy 6 KeV ...
**         delta-electrons notrace-delta-electrons 


  track heed	 
**  aval FIXED 20000
aval townsend

  prepare-track




  signal avalanche  noattachment diffusion ion-tail 
            average-signal 2 new

** convolute-signals range 0 1000 transfer-function (5*t/0.01)^5*exp(-5*t/0.01)
**  plot-signals
  write-signals  file="signalSr90_electron.dat" units microampere microsecond

enddo





&STOP


