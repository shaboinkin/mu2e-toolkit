####################################################
#	Probably an overly complicated approach        #
#   to multiprocessing...BUT IT SHALL BE DONE!!!   #
#												   #
# 	Created By: Daniel Kulas 					   #
#				7/17/12							   #
#			For: ConnectPrograms.sh      		   #
#                 mu2e							   #
####################################################


#THINGS TO NOTE!!!!!!!!!!!!
#	$1 = iterations parameter
#	$2 = signal selection parameter 
#
# 	$P0./RunGarf_P0.sh $iterations $sig_selection

#################################################
################set directories##################
mPATH=$HOME/mu2e_toolkit_3.0 	
mutliPATH=$mPATH/Multiprocess
P0=$mutliPATH/P0

rPATH=$mPATH/root
stoPATH=$rPATH/data_storage

gPATH=$P0/garfield/
fPATH=$P0/formatting

gFORMATTING=$fPATH/gFiles/Storage/
sFORMATTING=$fPATH/sFiles/Storage/

sPATH=$P0/spice/Preamp
sPATH_ELEC=$sPATH/electrons/
sPATH_PROT=$sPATH/protons/
sPATH_GAMM=$sPATH/gammas/
#################################################
########change energy files######################
GARF_FILE_E="signalSr90_electron.dat"			
GARF_FILE_G="signalSr90_gamma.dat"
GARF_FILE_P="signalSr90_proton.dat"

GARF_FILE_E_2="signalSr90_electron2.dat"
GARF_FILE_G_2="signalSr90_gamma2.dat"
GARF_FILE_P_2="signalSr90_proton2.dat"

GARF_FILE_E_NEW="signalSr90_electron_new.dat"
GARF_FILE_G_NEW="signalSr90_gamma_new.dat"
GARF_FILE_P_NEW="signalSr90_proton_new.dat"
#################################################
#################################################

SIGDAT="signal.dat"
OUT="output.dat"

#For now, TOTAL is used to randomly generate data to change the energy levels of garfield on the fly
TOTAL=105

#Removes old .dat files not needed anymore
#Be careful with these for future references; could potentially delete things you actually want to keep
#Always know what you plan to delete before you implement this.
find $gPATH -name '*.dat' -exec rm {} \;
find $gFORMATTING -name '*.dat' -exec rm {} \;
find $sFORMATTING -name '*.dat' -exec rm {} \;
find $sPATH_ELEC -name '*.dat' -exec rm {} \;
find $sPATH_GAMM -name '*.dat' -exec rm {} \;
find $sPATH_PROT -name '*.dat' -exec rm {} \;

#if signal selection = E || e
if [ "$2" = "E" -o "$2" = "e" ]; 
	then
		cd $gPATH
		echo "Please wait while data is being generated"
		#run this loop based on the amount of iterations ($1) you input at the start of the script
		for(( c=0; c<$1; c++))
		do 	
			#randomly generate different energy levels, 1 -> 105; $RANDOM is part of bash
			energy=$(( $RANDOM % TOTAL +1))
			#change the garfield .in file based on energy 
			./ModGarf_elec $energy
			#run garfield
			garfield-9 < "signalrndSr90_electron.in" >/dev/null 2>&1
			#remove first line of script
			#sleep 25
			tail -n +2 $GARF_FILE_E > $GARF_FILE_E_2
			
			#combine the outputted .dat file to a giant .dat file; keeps things together
			./CombineOutputs_elec $GARF_FILE_E_2
			rm $GARF_FILE_E_2
			rm $GARF_FILE_E
		done 
		#running the CombineOutputs file will change the name back into signalSr90_electron_new.dat
		if [ -f $GARF_FILE_E_NEW ]; 
			then
				#move file formatting

				mv $gPATH$GARF_FILE_E_NEW $gFORMATTING
				cd $gFORMATTING
				echo $GARF_FILE_E_NEW " ***************"
				#formate files to split them up into individual signals
				./FormatFiles $GARF_FILE_E_NEW
				#move original file back to where it was generated at, prevents errors
				mv $GARF_FILE_E_NEW $gPATH
				#move all .dat files from $gFORMATTING to $sFORMATTING to be processed

				#This block is for testing purposes but can be used if I, or whoever else works on this, can figure it out
				#Garfield produces a lot of junk when dealing with gamma signals and almost always produces no useable signals
				if [ -f 'signal1.dat' ]; then
					echo "Files found...continuing" 
				elif [ ! -f 'signal1.dat' ]; then
					echo "There are no files...aborting"
					exit -1
					bash
				else
					echo "Don't know how you got here..which can only mean, it was your fault."
					echo "That, or you found a super secret new feature!! Yeah!."
				fi

				#Formatting produces 'x' amount of signal.dat files. Move all of these over to be formatted for spice
				find $gFORMATTING -name '*.dat' -exec mv {} $sFORMATTING \;

				cd $sFORMATTING
				echo "Spicing it up P0"
				#grab a file with "[name].dat", read in the file and then...
				find ./ -type f -name \*.dat | while read file 
				do
					#format the file it grabbed to spice
					$sFORMATTING./Garfield2Spice $file 
					mv $SIGDAT $sPATH_ELEC
					cd $sPATH_ELEC

					#run ngspice
					#need the PID of the current spice program to kill it once it is done
					#otherwise, you just sit inside of spice, picking your nose waiting for something to happen
					ngspice Preamp.net  >/dev/null 2>&1  
					PID=$! 				>/dev/null 2>&1 		
					sleep .15			>/dev/null 2>&1 		
					kill -HUP $PID 		>/dev/null 2>&1 		

					#combine the output of spice to newoutput.dat (generate by CombineOutputs)
					./CombineOutputs output.dat
					cd $sFORMATTING		
				done 
				echo "Done spicing it P0"
				sleep .02
				#Not even sure if these sleeps are needed anymore, but I'll keep them in just in case...or cause I'm lazy. Either or.
		else
			echo "No .dat generated"
		fi

elif [ "$2" = "G" -o "$2" = "g" ];
	then
		cd $gPATH
		echo "Please wait while data is being generated"
		for(( c=1; c<=$1; c++))
		do 
			energy=$(( $RANDOM % TOTAL +1))
			./ModGarf_gamm $energy
			garfield-9 < "signalrndSr90_gamma.in" >/dev/null 2>&1
			tail -n +2 $GARF_FILE_G > $GARF_FILE_G_2
			./CombineOutputs_gamm $GARF_FILE_G_2
			rm $GARF_FILE_G_2
			rm $GARF_FILE_G
		done

		if [ -f $GARF_FILE_G_NEW ]; 
			then
				mv $gPATH$GARF_FILE_G_NEW $gFORMATTING
				cd $gFORMATTING

				./FormatFiles $GARF_FILE_G_NEW
				wait
				mv $GARF_FILE_G_NEW $gPATH

				if [ -f 'signal1.dat' ]; then
					echo "Files found...continuing" 
				elif [ ! -f 'signal1.dat' ]; then
					echo "There are no files...aborting"
					exit -1
					bash
				else
					echo "Don't know how you got here..which can only mean, it was your fault."
					echo "That, or you found a super secret new feature!! Yeah!."
				fi

				find $gFORMATTING -name '*.dat' -exec mv {} $sFORMATTING \;

				cd $sFORMATTING
				echo "Spicing it up P0"				
				find ./ -type f -name \*.dat | while read file

				do
					$sFORMATTING./Garfield2Spice $file 
					mv $SIGDAT $sPATH_GAMM
					cd $sPATH_GAMM

					ngspice Preamp.net  >/dev/null 2>&1  
					PID=$! 				>/dev/null 2>&1 		
					sleep .15			>/dev/null 2>&1 		
					kill -HUP $PID 		>/dev/null 2>&1 		

					./CombineOutputs output.dat
					cd $sFORMATTING						
				done 
				echo "Done spicing it P0"
				sleep .02	
		else
			echo "No .dat generated"
		fi

elif [ "$2" = "P" -o "$2" = "p" ];
	then
		cd $gPATH
		echo "Please wait while data is being generated"
		for(( c=1; c<=$1; c++))
		do 
			energy=$(( $RANDOM % TOTAL +1))
			./ModGarf_prot $energy
			garfield-9 < "signalrndSr90_proton.in" >/dev/null 2>&1
			tail -n +2 $GARF_FILE_P > $GARF_FILE_P_2
			./CombineOutputs_prot $GARF_FILE_P_2
			rm $GARF_FILE_P_2
			rm $GARF_FILE_P
		done 

		if [ -f $GARF_FILE_P_NEW ]; 
			then
				mv $gPATH$GARF_FILE_P_NEW $gFORMATTING
				cd $gFORMATTING

				./FormatFiles $GARF_FILE_P_NEW
				mv $GARF_FILE_P_NEW $gPATH
				
				if [ -f 'signal1.dat' ]; then
					echo "Files found...continuing" 
				elif [ ! -f 'signal1.dat' ]; then
					echo "There are no files...aborting"
					exit -1
					bash
				else
					echo "Don't know how you got here..which can only mean, it was your fault."
					echo "That, or you found a super secret new feature!! Yeah!."
				fi

				find $gFORMATTING -name '*.dat' -exec mv {} $sFORMATTING \;

				cd $sFORMATTING
				echo "Spicing it up P0"
				find ./ -type f -name \*.dat | while read file 
				do
					$sFORMATTING./Garfield2Spice $file 
					mv $SIGDAT $sPATH_PROT
					cd $sPATH_PROT

					ngspice Preamp.net  >/dev/null 2>&1  
					PID=$! 				>/dev/null 2>&1 		
					sleep .15			>/dev/null 2>&1 		
					kill -HUP $PID 		>/dev/null 2>&1 		

					./CombineOutputs output.dat
					cd $sFORMATTING						
				done 
				echo "Done spicing it P0"
				sleep .02	
		else
			echo "No .dat generated"
		fi
else
	echo "Something is wrong with your signal selection choice...aborting..."
	#exit codes are used in the event that the whole gamma shit thing works
	exit -1
	bash
fi

exit 0
bash 			
