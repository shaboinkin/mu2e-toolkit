/*	Code to format garfield output files so spice can read it
	
	Based on Garfield2Spice.cpp which used windows C++ functions
	Written by: (??) Aseet Mukherjee (??) 

	Coverted for gcc by: Daniel Kulas
						 7/6/12
						 Mu2e
															*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <fcntl.h>
#include <termios.h>
#include <stropts.h>
#include <unistd.h>
#include <math.h>

#define	MULTIFILE
#define SKIPZERO
#define KEEPTIME
#define bool int
#define true 1
#define false 0

//Code found here: http://forums.debian.net/viewtopic.php?f=8&t=59524 
int getch ( void ) {
  int ch;
  struct termios origterm, tmpterm;
  
  tcgetattr ( STDIN_FILENO, &origterm );
  tmpterm = origterm;
  tmpterm.c_lflag &= ~( ICANON | ECHO );
  tcsetattr ( STDIN_FILENO, TCSANOW, &tmpterm );
  ch = getchar();
  tcsetattr ( STDIN_FILENO, TCSANOW, &origterm );
  
  return ch;
  }
 

//Code found here: http://cboard.cprogramming.com/c-programming/63166-kbhit-linux.html
int kbhit(void)
{
  struct termios oldt, newt;
  int ch;
  int oldf;
 
  tcgetattr(STDIN_FILENO, &oldt);
  newt = oldt;
  newt.c_lflag &= ~(ICANON | ECHO);
  tcsetattr(STDIN_FILENO, TCSANOW, &newt);
  oldf = fcntl(STDIN_FILENO, F_GETFL, 0);
  fcntl(STDIN_FILENO, F_SETFL, oldf | O_NONBLOCK);
 
  ch = getchar();
 
  tcsetattr(STDIN_FILENO, TCSANOW, &oldt);
  fcntl(STDIN_FILENO, F_SETFL, oldf);
 
  if(ch != EOF)
  {
    ungetc(ch, stdin);
    return 1;
  }
 
  return 0;
}

//print
void Bye(char *message)
{
	puts(message);
	while(kbhit())
		getch();
	printf("\nPress any key to exit");
	while(!kbhit())
		usleep(100);
	exit(1);
}

int main(int argc, char* argv[])
{
	double tScale, vScale;
	double dtMin = 1.e-11;
//Input file
	char inpName[100] = "DriftSimOutput";	//change name
	if (argc > 1)
		strncpy(inpName, argv[1], sizeof(inpName));
	if(strchr(inpName, '.') == 0)
		strncat(inpName, ".dat", sizeof(inpName));
	FILE *inpFile = fopen(inpName, "rt");
	if(!inpFile)
		Bye("Input file not found");
//Output file
	char outBase[100],outName[100];
	strncpy(outBase, inpName, sizeof(outBase));
	char *pOutDot = strchr(outBase, '.');
	*pOutDot = 0;
#ifdef MULTIFILE
	int outCount = 0;
	FILE *outFile = 0;
#else
	strncpy(outName, outBase, sizeof(outName));
	strncat(outName, ".txt", sizeof(outName));
	FILE* outFile = fopen(outCount, "wt");
#endif

	char line[1000];
	int skip = 0;
	bool nonZero = false;
	int nOutLines = 0;
	double tOld = -1, tOff = 0, dt = 0;
	double vOld, vOff = 0;

	while(fgets(line, sizeof(line), inpFile))
	{
		double t, v;
		int nGot;
		char *pLine = strchr(line, '(');
		if(!pLine)
			pLine = strchr(line, '+');
		if(pLine)
			nGot = sscanf(pLine+1, "%le %le", &t, &v);
		else
			nGot = 0;
		if(nGot != 2)
		{
			if(skip < 0)
				skip = 0;
			if(nOutLines)
			{
				printf("%d lines spanning time %e to %e\n", nOutLines, tOff, tOld);
#ifdef MULTIFILE
				if(outFile)
				{
					fclose(outFile);
					outFile = 0;
					if(nonZero)
						outCount++;
					else
					{
						char command[100] = "delete ";
						strcat(command, outName);
						system(command);
					}
				}
#else
				tOff = tOld;
				vOff = vOld;
#endif
				nonZero = false;
				nOutLines = 0;
			}//end nOutLine
			if (strstr(line,"Cross-talk"))
				skip = 1;
			if (!skip && strstr(line,"Units used: ")) 
			{
				if (strstr(line,"micro second"))
					tScale = 1.e-6;
				else
					tScale = 1;
				if (strstr(line,"micro Ampere"))
					vScale = 1.e-6;
				else
					vScale = 1;
			}
			continue;
		}//end nGot
		if(skip)
		{
			skip = -1;
			continue;
		}

		t *= tScale;
		v *= vScale;
		if(abs(v) > 1.e-19)
			nonZero = true;
#ifndef	MULTIFILE
		t += tOff;
		v += vOff;
		dt = t - tOld;
		if(dt < dtMin)
			continue;
#endif
		vOff *= exp(-dt/1e-6);
		tOld = t;
		vOld = v;
#ifndef SKIPZERO
		if(!nonZero)
			continue;
#endif
		if(!outFile)
		{
			sprintf(outName, "signal.dat");
			outFile = fopen(outName, "wt");
		}

#ifdef KEEPTIME
		fprintf(outFile, "%e\t%e\n", t, v);
#else
		fprintf(outFile, "%e\n", v);
#endif
		nOutLines++;

	}//end while

	if(outFile)
		fclose(outFile);
	//Bye("Done");
	return 0;
}

