* source PREAMP5
*
* Preamp for straws
*
***************************************************************					
*	Infineon	Technologies	AG									
*	GUMMEL-POON	MODEL	IN	SPICE	2G6	SYNTAX				
*	VALID	UP	TO	10	GHZ							
*	>>>	BFP720ESD	<<<								
*	(C)	2010	Infineon	Technologies	AG							
*	Version 1.0	Juni	2010								
***************************************************************	
* - Please use the global SPICE parameter TEMP to set the junction
*   temperature of this device in your circuit to get correct DC 
*   simulation results. 
* - TEMP is calculated by TEMP=TA+P*(RthJS+RthSA). The junction 
*   temperature TEMP is the sum of the ambient temperature TA and 
*   the increment of temperature caused by the dissipated power 
*   P=VCE*IC (IC collector current, VCE collector-emitter voltage). 
* - RthJS is the thermal resistance between the junction and the 
*   soldering point. RthJS for this device is 415 K/W. RthSA is the 
*   thermal resistance of the PCB, from the soldering point to the 
*   ambient. For determination of RthSA please refer to Infineon's 
*   Application Note "Thermal Resistance Calculation" AN077.
* - The model has been verified in the junction temperature range
*   -25°C to +125°C.
* - TNOM=25 °C is the nominal ambient temperature.
*   Please do not change this value.
****************************************************************		
.OPTION TNOM=25, GMIN= 1.00e-12
*BFP720ESD C B E 
*$
.SUBCKT BFP720ESD 1 2 3 
CBEPAR 22 33 1.048E-013
CBCPAR 22 11 2.58E-014
CCEPAR 11 33 2.737E-013
LB    22 20 6.327E-010
LE   33 30 1.864E-010
LC   11 10  5.957E-010
CBEPCK 20 30  9.242E-014
CBCPCK 20 10  1.779E-015
CCEPCK 10 30  8.276E-014
LBX    20 2 3.338E-010
LEX   30 3 9.323E-011
LCX   10 1  2.42E-010
R_Tr 44 4 683.3
D1 33 25 M_D1  
D2 4 25  M_D2
RBLfdb 22 25 1.828
RPS 33 4 0.1123
RSUB 30 4 0.05469
D3 4 15 M_D3
D4 23 33 M_D4
D5 23 15 M_D5
RLDNBL 15 11 6.471
Q1 11 22 33 44 M_BFP720ESD
.MODEL M_D1 D(
+ IS=2.5E-017	
+ N=1.02
+ RS=6.1
+ CJO=1.968E-014)
.MODEL M_D2 D(
+ IS=2E-018	
+ N=1.02
+ RS=4170
+ CJO=4.284E-015)
.MODEL M_D3 D(
+ IS=3.5E-015	
+ N=1.02
+ RS=1380
+ CJO =9.378E-014)
.MODEL M_D4 D(
+ IS=3.5E-015	
+ N=1.02
+ RS=0.2
+ CJO =3.128E-014)
.MODEL M_D5 D(
+ IS=3.5E-015	
+ N=1.02
+ RS=4.7
+ CJO =5.321E-014)
.MODEL 	M_BFP720ESD	NPN(										
+	IS	=	7.612E-016									
+	BF	=	518.4									
+	NF	=	1.026									
+	VAF	=	157.5									
+	IKF	=	0.05529									
+	ISE	=	5.344E-015									
+	NE	=	1.829									
+	BR	=	264.6		
+	NR	=	0.9669		
+	VAR	=	2.278	
+	IKR	=	0.002409		
+	ISC	=	4.758E-015		
+	NC	=	1.568		
+	RB	=	8.442		
+	IRB	=	0		
+	RBM	=	0.1186	
+	RE	=	0.05132		
+	RC	=	2.182	
+	XTB	=	-2.1		
+	EG	=	1.11		
+	XTI	=	0.1		
+	CJE	=	5.895E-014		
+	VJE	=	1		
+	MJE	=	0.9539		
+	TF	=	2.521E-012		
+	XTF	=	17.49		
+	VTF	=	0.5295		
+	ITF	=	0.5638		
+	PTF	=	4.667		
+	CJC	=	8.027E-014		
+	VJC	=	0.4174		
+	MJC	=	0.3969		
+	XCJC	=	0.4894		
+	TR	=	1.793E-009
+	CJS	=	5.433E-014		
+	MJS	=	0.6481
+   VJS =   0.6332 		
+	FC	=	0.7712		
+	KF	=	1.264E-010		
+	AF	=	1.672)
***************************************************************					
.ENDS BFP720ESD
*
*
***************************************************************
* BFR181 NPN 
***********************
.SUBCKT BFR181 200 100 300
L1    1   10    0.85nH 
L2    2   20    0.001nH 
L3    3   30    0.69nH 
C1   10   20    84fF         
C2   20   30    165fF 
C3   30   10    73fF 
L4   10  100    0.51nH 
L5   20  200    0.49nH 
L6   30  300    0.61nH 
Q1   2 1 3 BFR181
.ENDS
.MODEL BFR181 NPN(
+ IS = 1.0519e-18     BF = 96.461       NF = 0.90617
+ VAF = 22.403        IKF = 0.12146     ISE = 1.2603e-14
+ NE = 1.7631         BR = 16.504       NR = 0.87757
+ VAR = 5.1127        IKR = 0.24951     ISC = 1.1195e-17
+ NC = 1.6528         RB = 9.9037       IRB = 0.00069278
+ RBM = 6.6315        RE = 2.1372       RC = 2.2171
+ CJE = 1.8168e-15    VJE = 0.73155     MJE = 0.43619
+ TF = 1.7028e-11     XTF = 0.33814     VTF = 0.12571 
+ ITF = 0.0010549     PTF = 0           CJC = 3.1969e-13
+ VJC = 1.1633        MJC = 0.30013     XCJC = 0.082903
+ TR = 2.7449e-09     CJS= 0            VJS = 0.75
+ MJS = 0             XTB = 0           EG = 1.11
+ XTI = 3             FC = 0.99768)

***************************************************************
*
*
* PREAMP CIRCUIT
*
*
***************************************************************
AV1 %i([N1323970 0]) filesrc
.model filesrc filesource (file="signal.dat" amploffset=[0 0] amplscale=[1 1]
+						   timeoffset=0 timescale=1
+						   timerelative=false amplstep=false)
*
*
R_R87         0 N1323970  110k  
C_C30         N1004939 N1004929  1p  
R_RoutA5      N1195685 N1004949  50  
V_V8       	  N1005055 0 2.5Vdc
R_R64         N1004939 N1004929  5.6k  
C_C24         0 N1004987  100n  
C_C25         0 N1005233  100n  
R_R65         N1004987 N1004939  1k  
C_C36         0 N1004929  5.1p  
X_Q30         N1005075 N1005075 0 BFR181
X_Q32         N1004949 N1005233 N1004899 BFR181
R_RoutB5      N1195685 N1004933  50  
R_R46         N1005075 N1005055  500  
X_Q33         N1004933 N1004939 N1004899 BFR181
C_C35         0 N1312121  3p  
X_Q31         N1004899 N1005075 0 BFR181
R_R49         N1004929 N1005055  330  
R_R66         N1005233 N1004929  5.6k  
R_R47         N1005587 N1004929  5.6k  
R_R62         N1202523 N1312121  20  
X_Q29         N1004929 N1005587 0 BFP720ESD
R_R80         0 N1005587  6.8k  
R_R63         N1312121 N1005587  20  
V_V9      	  N1195685 0 2.5Vdc
C_Cin5        N1323970 N1202523  1n  
*
*
*
.tran 0.1ns 0.1us
.control
set filetype=ascii
run
write output.dat N1004949
*plot N1004949
.endc
*
*
.END
