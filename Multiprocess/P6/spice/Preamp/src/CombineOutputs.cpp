/*************************** CombineOutputs.cpp *************************
 *  					                                                *                        
 *                                                                      *                        
 *   Simple program to to write the spice outputdata to the last line	*				            
 *   of a file. Pass in output.dat from spice as your arguments         * 	                       
 *                                                                      *                          
 *                                                                      *                        
 *   Created by: Daniel Kulas    										*						
 *				 7/9/12     				                            *                        
 *               mu2e                                                   * 
 *              	                                                    *                         
 ************************************************************************/

#include <fstream>
#include <string>
#include <iostream>
#include <sstream>
#include <cstring>
#include <stdlib.h>
				
using namespace std;

int main(int argc, char* argv[])
{
	if(argc < 1)
	{
		cout << "Error: No file was passed in...abort" << endl;
		exit(0);
	}

	string line;
	char theFile[100];
	strcpy(theFile, argv[1]);
	ifstream input;
	ofstream output("newoutput.dat", fstream::in | fstream::out | fstream::app);	//scary voodoo black magic happens here

	input.open("output.dat"); 

	while(!input.eof())
	{
		getline(input, line);
		output << line << endl;
	}

	input.close();
	output.close();
}
